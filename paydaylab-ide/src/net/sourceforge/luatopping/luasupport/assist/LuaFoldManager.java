/*
 *  Copyright (C) 2012 Arcanefoam (Horacio Hoyos) <arcanefoam at gmail dot com>
 * 
 *  This file is part of luaSupport
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.sourceforge.luatopping.luasupport.assist;

import javax.swing.event.DocumentEvent;
import org.netbeans.api.editor.fold.Fold;
import org.netbeans.spi.editor.fold.FoldHierarchyTransaction;
import org.netbeans.spi.editor.fold.FoldManager;
import org.netbeans.spi.editor.fold.FoldOperation;

/**
 *
 * @author Arcanefoam (Horacio Hoyos) <arcanefoam at gmail dot com>
 */
public class LuaFoldManager implements FoldManager {

    private FoldOperation fo;
    
    public LuaFoldManager() {
           System.out.println("LuaFoldManager initialized");
    }

    @Override
    public void init(FoldOperation fo) {
        this.fo = fo;
    }

    @Override
    public void initFolds(FoldHierarchyTransaction fht) {
        /* Any listeners necessary for the maintenance of the folds can be attached here. */
        System.out.println("LuaFoldManager initFolds");
        
    }

    @Override
    public void insertUpdate(DocumentEvent de, FoldHierarchyTransaction fht) {
        System.out.println("LuaFoldManager insertUpdate");
    }

    @Override
    public void removeUpdate(DocumentEvent de, FoldHierarchyTransaction fht) {
        System.out.println("LuaFoldManager removeUpdate");
    }

    @Override
    public void changedUpdate(DocumentEvent de, FoldHierarchyTransaction fht) {
        System.out.println("LuaFoldManager changedUpdate");
    }

    @Override
    public void removeEmptyNotify(Fold fold) {
        System.out.println("LuaFoldManager removeEmptyNotify");
    }

    @Override
    public void removeDamagedNotify(Fold fold) {
        System.out.println("LuaFoldManager removeDamagedNotify");
    }

    @Override
    public void expandNotify(Fold fold) {
        System.out.println("LuaFoldManager expandNotify");
    }

    @Override
    public void release() {
        System.out.println("LuaFoldManager release");
    }
}
