/*
 *  Copyright (C) 2012 Arcanefoam (Horacio Hoyos) <arcanefoam at gmail dot com>
 * 
 *  This file is part of lusSupport
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.sourceforge.luatopping.luasupport.parser;

import java.util.ArrayList;
import java.util.List;
import javax.swing.text.Document;
import net.sourceforge.luatopping.luasupport.parser.LuaSupportParser.LuaParserResult;
import org.antlr.v4.runtime.Token;
import org.netbeans.modules.parsing.spi.Parser.Result;
import org.netbeans.modules.parsing.spi.ParserResultTask;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.spi.editor.hints.ErrorDescription;
import org.netbeans.spi.editor.hints.ErrorDescriptionFactory;
import org.netbeans.spi.editor.hints.HintsController;
import org.netbeans.spi.editor.hints.Severity;

/**
 * Add syntax error highlights.
 *
 * @author Arcanefoam (Horacio Hoyos) <arcanefoam at gmail dot com>
 * @author Campbell Suter <znix@znix.xyz>
 */
class LuaSyntaxErrorsHighlightingTask extends ParserResultTask {

	public LuaSyntaxErrorsHighlightingTask() {
	}

	@Override
	public void run(Result result, SchedulerEvent event) {
		LuaParserResult luaResult = (LuaParserResult) result;
		List<ParseError> syntaxErrors = luaResult.getDiagnostics();
		Document document = result.getSnapshot().getSource().getDocument(false);
		List<ErrorDescription> errors = new ArrayList<>();
		for (ParseError syntaxError : syntaxErrors) {
			int line;
			if (syntaxError.getException() != null) {
				Token tok = syntaxError.getException().getOffendingToken();

				line = tok.getLine();
			} else {
				line = syntaxError.getLine();
			}

			if (line <= 0) {
				continue;
			}

			String message = syntaxError.getDisplayName();
			ErrorDescription errorDescription = ErrorDescriptionFactory.createErrorDescription(
					Severity.ERROR,
					message,
					document,
					line);
			errors.add(errorDescription);
		}
		HintsController.setErrors(document, "lua", errors);
	}

	@Override
	public int getPriority() {
		return 100;
	}

	@Override
	public Class<? extends Scheduler> getSchedulerClass() {
		return Scheduler.EDITOR_SENSITIVE_TASK_SCHEDULER;
	}

	@Override
	public void cancel() {
	}

}
