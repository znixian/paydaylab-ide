/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.sourceforge.luatopping.luasupport.parser;

import org.antlr.v4.runtime.RecognitionException;
import org.netbeans.modules.csl.api.Error;
import org.netbeans.modules.csl.api.Severity;
import org.openide.filesystems.FileObject;

/**
 * The basic definition of a Lua parse (syntax) error.
 *
 * @author ZNix <znix@znix.xyz>
 */
public class ParseError implements Error {

	private final String displayName;
	private final RecognitionException ex;
	private final int line;
	private final int charPositionInLine;

	public ParseError(RecognitionException ex, String displayName,
			int line, int charPositionInLine) {
		this.ex = ex;
		this.displayName = displayName;
		this.line = line;
		this.charPositionInLine = charPositionInLine;
	}

	public RecognitionException getException() {
		return ex;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}

	public int getLine() {
		return line;
	}

	public int getCharPositionInLine() {
		return charPositionInLine;
	}

	@Override
	public String getDescription() {
		return null;
	}

	@Override
	public String getKey() {
		return null;
	}

	@Override
	public FileObject getFile() {
		return null;
	}

	@Override
	public int getStartPosition() {
		return -1;
	}

	@Override
	public int getEndPosition() {
		return -1;
	}

	@Override
	public boolean isLineError() {
		return false;
	}

	@Override
	public Severity getSeverity() {
		return Severity.ERROR;
	}

	@Override
	public Object[] getParameters() {
		return null;
	}

}
