/*
 *  Copyright (C) 2012 Arcanefoam (Horacio Hoyos) <arcanefoam at gmail dot com>
 * 
 *  This file is part of lusSupport
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.sourceforge.luatopping.luasupport.parser;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import javax.swing.event.ChangeListener;
import net.sourceforge.luatopping.luasupport.antlr.LuaLexer;
import net.sourceforge.luatopping.luasupport.antlr.LuaParser;
import org.antlr.v4.runtime.ANTLRErrorListener;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.atn.ATNConfigSet;
import org.antlr.v4.runtime.dfa.DFA;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Task;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.SourceModificationEvent;

/**
 * Parser wrapper for Lua files.
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class LuaSupportParser extends Parser {

	private Snapshot snapshot;
	private LuaParser luaParser;
	private List<ParseError> errors;

	@Override
	public void parse(Snapshot snapshot, Task task, SourceModificationEvent event) {
		this.snapshot = snapshot;
		errors = new ArrayList<>();
		ANTLRInputStream input = new ANTLRInputStream(snapshot.getText().toString());
		Lexer lexer = new LuaLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		luaParser = new LuaParser(tokens);
		luaParser.addErrorListener(new ErrorHandler());
		try {
			luaParser.block();
		} catch (RecognitionException ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public Result getResult(Task task) {
		return new LuaParserResult(snapshot, luaParser, errors);
	}

	@Override
	public void cancel() {
	}

	@Override
	public void addChangeListener(ChangeListener changeListener) {
	}

	@Override
	public void removeChangeListener(ChangeListener changeListener) {
	}

	private class ErrorHandler implements ANTLRErrorListener {

		@Override
		public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol,
				int line, int charPositionInLine, String msg, RecognitionException e) {
			errors.add(new ParseError(e, msg, line, charPositionInLine));
		}

		@Override
		public void reportAmbiguity(org.antlr.v4.runtime.Parser parser, DFA dfa,
				int i, int i1, boolean bln, BitSet bitset, ATNConfigSet atncs) {
			// TODO implement
			//throw new UnsupportedOperationException("Not supported yet.");
		}

		@Override
		public void reportAttemptingFullContext(org.antlr.v4.runtime.Parser parser,
				DFA dfa, int i, int i1, BitSet bitset, ATNConfigSet atncs) {
			// TODO implement
			//throw new UnsupportedOperationException("Not supported yet.");
		}

		@Override
		public void reportContextSensitivity(org.antlr.v4.runtime.Parser parser,
				DFA dfa, int i, int i1, int i2, ATNConfigSet atncs) {
			// TODO implement
			//throw new UnsupportedOperationException("Not supported yet.");
		}

	}

	public static class LuaParserResult extends ParserResult {

		private final LuaParser parser;
		private final List<ParseError> errors;
		private boolean valid = true;

		private LuaParserResult(Snapshot snapshot, LuaParser parser,
				List<ParseError> errors) {
			super(snapshot);
			this.parser = parser;
			this.errors = errors;
		}

		public LuaParser getLuaParser()
				throws ParseException {
			if (!valid) {
				throw new ParseException();
			}
			return parser;
		}

		@Override
		protected void invalidate() {
			valid = false;
		}

		@Override
		public List<ParseError> getDiagnostics() {
			return errors;
		}
	}
}
