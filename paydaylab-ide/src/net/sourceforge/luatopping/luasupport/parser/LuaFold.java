/*
 *  Copyright (C) 2010 Arcanefoam (Horacio Hoyos) <arcanefoam at gmail dot com>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.sourceforge.luatopping.luasupport.parser;

import org.netbeans.api.editor.fold.FoldType;

/**
 *
 * @author Arcanefoam (Horacio Hoyos) <arcanefoam at gmail dot com>
 */
public class LuaFold implements Comparable<LuaFold> {

    private FoldType type;
    private String description;
    private boolean collapsed = false;
    private int startOffset;
    private int endOffset;
    private int startGuardedLength;
    private int endGuardedLength;
    private Object extraInfo;

    public LuaFold(String description, int startOffset, int startGuardedLength, int endGuardedLength) {
        this.description = description;
        this.startOffset = startOffset;
        this.startGuardedLength = startGuardedLength;
        this.endGuardedLength = endGuardedLength;
        this.type = new FoldType(description);
    }

    public void setEndOffset(int endOffset) {
        this.endOffset = endOffset;
    }

    public boolean isMultiLine() {
        return (endOffset > startOffset);
    }

    public boolean isCollapsed() {
        return collapsed;
    }

    public String getDescription() {
        return description;
    }

    public int getEndGuardedLength() {
        return endGuardedLength;
    }

    public int getEndOffset() {
        return endOffset;
    }

    public Object getExtraInfo() {
        return extraInfo;
    }

    public int getStartGuardedLength() {
        return startGuardedLength;
    }

    public int getStartOffset() {
        return startOffset;
    }

    public FoldType getType() {
        return type;
    }


    @Override
    public int compareTo(LuaFold o) {
        return ((Integer)startOffset).compareTo((Integer)o.startOffset);
    }

}
