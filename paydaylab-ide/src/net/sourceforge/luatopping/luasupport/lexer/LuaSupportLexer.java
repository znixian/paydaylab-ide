/*
 *  Copyright (C) 2012 Arcanefoam (Horacio Hoyos) <arcanefoam at gmail dot com>
 * 
 *  This file is part of lusSupport
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.sourceforge.luatopping.luasupport.lexer;

import net.sourceforge.luatopping.luasupport.antlr.LuaLexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.misc.Interval;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerInput;
import org.netbeans.spi.lexer.LexerRestartInfo;

/**
 *
 * @author Arcanefoam (Horacio Hoyos) <arcanefoam at gmail dot com>
 */
public class LuaSupportLexer implements Lexer<LuaSupportTokenId> {

	private final LexerRestartInfo<LuaSupportTokenId> info;

	private final LuaLexer luaLexer;

	public LuaSupportLexer(LexerRestartInfo<LuaSupportTokenId> info) {
		this.info = info;

		AntlrCharStream charStream = new AntlrCharStream(info.input());
		luaLexer = new LuaLexer(charStream);
	}

	@Override
	public org.netbeans.api.lexer.Token<LuaSupportTokenId> nextToken() {
		Token token = luaLexer.nextToken();
		if (token.getType() != LuaLexer.EOF) {
			LuaSupportTokenId tokenId = LuaLanguageHierarchy.getToken(token.getType());
			return info.tokenFactory().createToken(tokenId);
		}

		String text = info.input().readText().toString();
		if (!text.isEmpty()) {
			LuaSupportTokenId tokenId = LuaLanguageHierarchy.getToken(LuaLanguageHierarchy.WS_ID);
			return info.tokenFactory().createToken(tokenId);
		}

		return null;
	}

	@Override
	public Object state() {
		return null;
	}

	@Override
	public void release() {
	}

	private final class AntlrCharStream implements CharStream {

		private final LexerInput input;
		private int index = 0;
		private int markDepth = 0;

		public AntlrCharStream(LexerInput input) {
			this.input = input;
		}

		@Override
		public void consume() {
			int character = read();
			if (character == EOF) {
				backup(1);
				throw new IllegalStateException("Attempting to consume EOF");
			}
			index++;
		}

		@Override
		public int LA(int lookaheadAmount) {
			if (lookaheadAmount < 0) {
				return lookBack(-lookaheadAmount);
			} else if (lookaheadAmount > 0) {
				return lookAhead(lookaheadAmount);
			} else {
				return 0; //Behaviour is undefined when lookaheadAmount == 0
			}
		}

		private int lookBack(int amount) {
			backup(amount);
			int character = read();
			for (int i = 1; i < amount; i++) {
				read();
			}
			return character;
		}

		private int lookAhead(int amount) {
			int character = 0;
			for (int i = 0; i < amount; i++) {
				character = read();
			}
			backup(amount);
			return character;
		}

		@Override
		public int mark() {
			return ++markDepth;
		}

		@Override
		public void release(int marker) {
			// unwind any other markers made after m and release m
			markDepth = marker;
			// release this marker
			markDepth--;
		}

		@Override
		public void seek(int index) {
			if (index < 0) {
				throw new IllegalArgumentException(String.format("Invalid index (%s < 0)", index));
			}

			if (index < this.index) {
				backup(this.index - index);
				this.index = index;
				return;
			}

			// seek forward, consume until p hits index
			while (this.index < index) {
				consume();
			}
		}

		@Override
		public int index() {
			return index;
		}

		@Override
		public int size() {
			throw new UnsupportedOperationException("Stream size unknown");
		}

		private int read() {
			int result = input.read();
			if (result == LexerInput.EOF) {
				return EOF;
			} else {
				return result;
			}
		}

		private void backup(int count) {
			input.backup(count);
		}

		@Override
		public String getText(Interval iv) {
			int len = iv.b - iv.a;
			char[] chars = new char[len];

			seek(iv.a);
			for (int i = 0; i < len; i++) {
				int status = read();
				if (status == LexerInput.EOF) {
					throw new IllegalStateException("Reached EOF while reading string");
				}
				chars[i] = (char) status;
			}

			return String.valueOf(chars);
		}

		@Override
		public String getSourceName() {
			throw new UnsupportedOperationException("Not supported yet.");
		}
	}
}
