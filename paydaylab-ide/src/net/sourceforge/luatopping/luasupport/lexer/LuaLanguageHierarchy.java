/*
 *  Copyright (C) 2012 Arcanefoam (Horacio Hoyos) <arcanefoam at gmail dot com>
 * 
 *  This file is part of lusSupport
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.sourceforge.luatopping.luasupport.lexer;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.netbeans.spi.lexer.LanguageHierarchy;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerRestartInfo;

/**
 *
 * @author Arcanefoam (Horacio Hoyos) <arcanefoam at gmail dot com>
 */
public class LuaLanguageHierarchy extends LanguageHierarchy<LuaSupportTokenId> {
	
	public static final int WS_ID = 66;

	private static List<LuaSupportTokenId> tokens;
	private static Map<Integer, LuaSupportTokenId> idToToken;

	private static void init() {
		int i;
		tokens = Arrays.<LuaSupportTokenId>asList(new LuaSupportTokenId[]{
			new LuaSupportTokenId("NAME", "identifier", 56),
			new LuaSupportTokenId("NORMALSTRING", "string", 57),
			new LuaSupportTokenId("CHARSTRING", "string", 58),
			new LuaSupportTokenId("LONGSTRING", "string", 59),
			new LuaSupportTokenId("INT", "number", 60),
			new LuaSupportTokenId("HEX", "number", 61),
			new LuaSupportTokenId("FLOAT", "number", 62),
			new LuaSupportTokenId("HEX_FLOAT", "number", 63),
			new LuaSupportTokenId("COMMENT", "comment", 64),
			new LuaSupportTokenId("LINE_COMMENT", "comment", 65),
			new LuaSupportTokenId("WS", "whitespace", 66),
			new LuaSupportTokenId("SHEBANG", "comment", 67),
			new LuaSupportTokenId("';'", "separator", 1),
			new LuaSupportTokenId("'='", "separator", 2),
			new LuaSupportTokenId("'break'", "keyword", 3),
			new LuaSupportTokenId("'goto'", "keyword", 4),
			new LuaSupportTokenId("'do'", "keyword", 5),
			new LuaSupportTokenId("'end'", "keyword", 6),
			new LuaSupportTokenId("'while'", "keyword", 7),
			new LuaSupportTokenId("'repeat'", "keyword", 8),
			new LuaSupportTokenId("'until'", "keyword", 9),
			new LuaSupportTokenId("'if'", "keyword", 10),
			new LuaSupportTokenId("'then'", "keyword", 11),
			new LuaSupportTokenId("'elseif'", "keyword", 12),
			new LuaSupportTokenId("'else'", "keyword", 13),
			new LuaSupportTokenId("'for'", "keyword", 14),
			new LuaSupportTokenId("','", "seperator", 15),
			new LuaSupportTokenId("'in'", "keyword", 16),
			new LuaSupportTokenId("'function'", "keyword", 17),
			new LuaSupportTokenId("'local'", "keyword", 18),
			new LuaSupportTokenId("'return'", "keyword", 19),
			new LuaSupportTokenId("'::'", "separator", 20),
			new LuaSupportTokenId("'.'", "separator", 21),
			new LuaSupportTokenId("':'", "separator", 22),
			new LuaSupportTokenId("'nil'", "keyword", 23),
			new LuaSupportTokenId("'false'", "keyword", 24),
			new LuaSupportTokenId("'true'", "keyword", 25),
			new LuaSupportTokenId("'...'", "operator", 26),
			new LuaSupportTokenId("'('", "separator", 27),
			new LuaSupportTokenId("')'", "separator", 28),
			new LuaSupportTokenId("'['", "separator", 29),
			new LuaSupportTokenId("']'", "separator", 30),
			new LuaSupportTokenId("'{'", "separator", 31),
			new LuaSupportTokenId("'}'", "separator", 32),
			new LuaSupportTokenId("'or'", "keyword", 33),
			new LuaSupportTokenId("'and'", "keyword", 34),
			new LuaSupportTokenId("'<'", "separator", 35),
			new LuaSupportTokenId("'>'", "separator", 36),
			new LuaSupportTokenId("'<='", "separator", 37),
			new LuaSupportTokenId("'>=", "separator", 38),
			new LuaSupportTokenId("'~=", "separator", 39),
			new LuaSupportTokenId("'=='", "separator", 40),
			new LuaSupportTokenId("'..'", "separator", 41),
			new LuaSupportTokenId("'+'", "operator", 42),
			new LuaSupportTokenId("'-'", "operator", 43),
			new LuaSupportTokenId("'*'", "operator", 44),
			new LuaSupportTokenId("'/'", "operator", 45),
			new LuaSupportTokenId("'%'", "operator", 46),
			new LuaSupportTokenId("'//'", "operator", 47),
			new LuaSupportTokenId("'&'", "operator", 48),
			new LuaSupportTokenId("'|'", "operator", 49),
			new LuaSupportTokenId("'~'", "operator", 50),
			new LuaSupportTokenId("'<<'", "operator", 51),
			new LuaSupportTokenId("'>>'", "operator", 52),
			new LuaSupportTokenId("'not'", "operator", 53),
			new LuaSupportTokenId("'#'", "comment", 54),
			new LuaSupportTokenId("'^'", "operator", 55)
		});
		idToToken = new HashMap<>();

		tokens.forEach(t
				-> idToToken.put(t.ordinal(), t));
	}

	static synchronized LuaSupportTokenId getToken(int id) {
		if (idToToken == null) {
			init();
		}
		return idToToken.get(id);
	}

	@Override
	protected Collection<LuaSupportTokenId> createTokenIds() {
		if (tokens == null) {
			init();
		}
		return tokens;
	}

	@Override
	protected Lexer<LuaSupportTokenId> createLexer(LexerRestartInfo<LuaSupportTokenId> lri) {
		return new LuaSupportLexer(lri);
	}

	@Override
	protected String mimeType() {
		return "text/x-lua";
	}

}
