/*
 *  Copyright (C) 2012 Arcanefoam (Horacio Hoyos) <arcanefoam at gmail dot com>
 * 
 *  This file is part of lusSupport
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.sourceforge.luatopping.luasupport;

import net.sourceforge.luatopping.luasupport.lexer.LuaSupportTokenId;
import net.sourceforge.luatopping.luasupport.parser.LuaSupportParser;
//import net.sourceforge.luatopping.luasupport.parser.LuaSupportParser;
import org.netbeans.api.lexer.Language;
import org.netbeans.modules.csl.spi.DefaultLanguageConfig;
import org.netbeans.modules.csl.spi.LanguageRegistration;
import org.netbeans.modules.parsing.spi.Parser;

/**
 *
 * @author Arcanefoam (Horacio Hoyos) <arcanefoam at gmail dot com>
 */
@LanguageRegistration(mimeType = "text/x-lua")
public class LuaLanguage extends DefaultLanguageConfig {
    
    @Override
    public Language<LuaSupportTokenId> getLexerLanguage() {
        return LuaSupportTokenId.getLanguage();
    }

    @Override
    public String getDisplayName() {
        return "lua";
    }

    @Override
    public Parser getParser() {
        return new LuaSupportParser();
    }
    
}
