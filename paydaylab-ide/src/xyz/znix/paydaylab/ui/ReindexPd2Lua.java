/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.paydaylab.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle.Messages;
import xyz.znix.paydaylab.decomp.SourceDB;

@ActionID(
        category = "Tools",
        id = "xyz.znix.paydaylab.ui.ReindexPd2Lua"
)
@ActionRegistration(
        displayName = "#CTL_ReindexPd2Lua"
)
@ActionReference(path = "Menu/Source", position = 9100)
@Messages("CTL_ReindexPd2Lua=Reindex PAYDAY 2 Lua")
public final class ReindexPd2Lua implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        SourceDB.get(true);
    }
}
