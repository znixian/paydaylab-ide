/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.paydaylab.decomp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.prefs.Preferences;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import org.netbeans.api.progress.ProgressHandle;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;
import org.openide.util.RequestProcessor;
import xyz.znix.paydaylab.options.PaydayPanel;

/**
 *
 * @author ZNix <znix@znix.xyz>
 */
public class SourceDB {

	private final static RequestProcessor REQ_PROC
			= new RequestProcessor("pd2lua indexing", 1, true);
	private static final Pattern CLASS_MATCHER = Pattern.compile(
			"\\s*(\\w+)\\s*=\\s*(?:\\w+\\s*or)?\\s*class\\s*\\((\\s*(?:\\w*\\.)?\\w*\\s*)\\)\\s*"
	);

	private static SourceDB instance;
	private static boolean loading;

	private final Map<String, LuaClass> classes;
	private final List<File> files;

	private SourceDB() throws IOException, InterruptedException {
		classes = new HashMap<>();
		files = new ArrayList<>();

		List<PendingClass> pending = new ArrayList<>();
		crawlDirectory(getSourcesRoot(), pending);

		Map<String, PendingClass> byName = new HashMap<>();
		pending.forEach(pc -> byName.put(pc.name, pc));

		while (!byName.isEmpty()) {
			// TODO more efficent
			String name = byName.keySet().iterator().next();
			getOrFinalizeClass(name, byName);
		}
	}

	public void grep(String text, boolean matchCase, Consumer<SearchResult> cb) {
		for (File file : files) {
			try (BufferedReader br = new BufferedReader(
					new InputStreamReader(new FileInputStream(file)))) {
				String line;
				int linenum = 0;
				while ((line = br.readLine()) != null) {
					int index;
					if (matchCase) {
						index = line.indexOf(text);
					} else {
						index = line.toLowerCase().indexOf(text.toLowerCase());
					}
					if (index != -1) {
						cb.accept(new SearchResult(file, linenum, index, line));
					}
					linenum++;
				}
			} catch (FileNotFoundException ex) {
				// But this file was here earlier? WTF?
				Exceptions.printStackTrace(ex);
			} catch (IOException ex) {
				Exceptions.printStackTrace(ex);
			}
		}
	}

	private LuaClass getOrFinalizeClass(String name, Map<String, PendingClass> byName) {
		return classes.computeIfAbsent(name, __ -> {
			PendingClass pc = byName.remove(name);
			LuaClass klass;
			if (pc != null) {
				LuaClass parent = pc.parent == null ? null : getOrFinalizeClass(pc.parent, byName);
				klass = new LuaClass(name, parent, pc.source);
			} else {
				klass = null;
				System.out.println("Missing class " + name);
			}
			classes.put(name, klass);
			return klass;
		});
	}

	public LuaClass getLuaClass(String name) {
		return classes.get(name);
	}

	private void crawlDirectory(File directory, List<PendingClass> classes) throws IOException, InterruptedException {
		for (File file : directory.listFiles()) {
			// Exclude SCMs, for example
			if (file.getName().startsWith(".")) {
				continue;
			}

			if (Thread.interrupted()) {
				throw new InterruptedException();
			}

			if (file.isFile()) {
				if (file.getName().endsWith(".lua")) {
					parseFile(file, classes);
				}
			} else {
				crawlDirectory(file, classes);
			}
		}
	}

	private void parseFile(File file, List<PendingClass> classes) throws IOException {
		files.add(file);

		// System.out.println("Scanning file " + file);
		// TODO do this properly
		try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
			String line;
			while ((line = reader.readLine()) != null) {
				line = line.trim();

				Matcher matcher = CLASS_MATCHER.matcher(line);

				if (!matcher.matches()) {
					continue;
				}

				String name = matcher.group(1);
				String parent = matcher.group(2);
				if (parent.isEmpty()) {
					parent = null;
				}

				PendingClass pc = new PendingClass();
				classes.add(pc);
				pc.name = name;
				pc.parent = parent;
				pc.source = file;

				// System.out.println("Class " + name + "<" + parent);
			}
		}
	}

	private static void loadAsync() {
		try {
			instance = new SourceDB();
		} catch (IOException ex) {
			throw new RuntimeException("Exception while parsing lua", ex);
		} catch (InterruptedException ex) {
			// Task was cancelled, nothing to see here.
		}
		loading = false;
	}

	@NbBundle.Messages("LBL_Lua_Payday_Index=Indexing PAYDAY 2 Lua")
	public static SourceDB get(boolean reload) {
		// If we're in the middle of reindexing, use what we have
		if (loading == true) {
			return instance;
		}

		// Use our cached copy if possible
		if (instance != null && !reload) {
			return instance;
		}

		// If the PD2 directory does not exist
		if (getSourcesRoot() == null) {
			JOptionPane.showMessageDialog(null, "The decompiled PD2 sources are missing."
					+ "\nPlease check it's path in the options menu.",
					"Decompiled Sources Missing", JOptionPane.ERROR_MESSAGE);
			return null;
		}

		loading = true;

		final RequestProcessor.Task task = REQ_PROC.create(SourceDB::loadAsync);

		final ProgressHandle ph = ProgressHandle.createHandle(
				Bundle.LBL_Lua_Payday_Index(), task);

		task.addTaskListener(finished -> {
			//make sure that we get rid of the ProgressHandle
			//when the task is finished
			ph.finish();
		});

		//start the progresshandle the progress UI will show 500s after
		ph.start();

		//this actually start the task
		task.schedule(0);

		// Use what we have.
		return instance;
	}

	public static File getSourcesRoot() {
		Preferences prefs = NbPreferences.forModule(PaydayPanel.class);
		File path = new File(prefs.get("decompSrcPath", ""));
		return path.exists() ? path : null;
	}

	private static class PendingClass {

		private String name;
		private String parent;
		private File source;
	}

	public static class SearchResult {

		private final File file;
		private final int line;
		private final int column;

		private final String contents;

		private SearchResult(File file, int line, int column, String contents) {
			this.file = file;
			this.line = line;
			this.column = column;
			this.contents = contents;
		}

		public File getFile() {
			return file;
		}

		public int getLine() {
			return line;
		}

		public int getColumn() {
			return column;
		}

		public String getContents() {
			return contents;
		}

		@Override
		public String toString() {
			String name = file.getName();
			return name.substring(0, name.length() - 4) + ": " + contents;
		}
	}
}
