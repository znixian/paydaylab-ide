/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.paydaylab.decomp;

import java.io.File;

/**
 *
 * @author ZNix <znix@znix.xyz>
 */
public class LuaClass {

    private final String name;
    private final LuaClass parent;
    private final File source;

    public LuaClass(String name, LuaClass parent, File source) {
        this.name = name;
        this.parent = parent;
        this.source = source;
    }

    public String getName() {
        return name;
    }

    public LuaClass getParent() {
        return parent;
    }

    public File getSource() {
        return source;
    }

}
