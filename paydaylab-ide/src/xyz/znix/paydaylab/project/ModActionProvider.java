/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.paydaylab.project;

import java.awt.event.ActionEvent;
import java.util.Arrays;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import org.json.simple.JSONObject;
import org.netbeans.api.annotations.common.StaticResource;
import org.netbeans.spi.project.ActionProvider;
import org.openide.awt.NotificationDisplayer;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import xyz.znix.paydaylab.run.PaydayRunnerTask;

/**
 * A provider for run, debug, etc. actions.
 *
 * @author ZNix <znix@znix.xyz>
 */
public class ModActionProvider implements ActionProvider {

	@StaticResource()
	public static final String MOD_ICON = "xyz/znix/paydaylab/reload_icon_16.png";

	private static Icon RELOAD_ICON;

	private static final String[] SUPPORTED_ACTIONS = {
		ActionProvider.COMMAND_RUN,
		ActionProvider.COMMAND_DEBUG
	};

	private final ModProject mod;

	public ModActionProvider(ModProject mod) {
		this.mod = mod;
	}

	@Override
	public String[] getSupportedActions() {
		return SUPPORTED_ACTIONS;
	}

	@Override
	public void invokeAction(String action, Lookup lkp) throws IllegalArgumentException {
		if (!Arrays.asList(SUPPORTED_ACTIONS).contains(action)) {
			throw new IllegalArgumentException("Illegal action " + action);
		}

		switch (action) {
			case ActionProvider.COMMAND_RUN: {
				if (PaydayRunnerTask.isRunning()) {
					JSONObject obj = new JSONObject();
					obj.put("op", "reload");
					PaydayRunnerTask.sendLuaMessage(obj.toJSONString());

					if (RELOAD_ICON == null) {
						RELOAD_ICON = new ImageIcon(ImageUtilities.loadImage(MOD_ICON));
					}

					// TODO i18n
					NotificationDisplayer.getDefault().notify("Reloaded PD2", RELOAD_ICON,
							"PAYDAY 2 should be reloading the current state", this::handleReloadClick);
				} else {
					PaydayRunnerTask.start(false);
				}
				break;
			}
			case ActionProvider.COMMAND_DEBUG: {
				PaydayRunnerTask.startDebug();
			}
		}
	}

	@Override
	public boolean isActionEnabled(String action, Lookup lkp) throws IllegalArgumentException {
		// Can't start debugging while running
		if (ActionProvider.COMMAND_DEBUG.equals(action) && PaydayRunnerTask.isRunning()) {
			return false;
		}
		return Arrays.asList(SUPPORTED_ACTIONS).contains(action);
	}

	private void handleReloadClick(ActionEvent ev) {
		// TODO: Switch to PD2 window
	}

}
