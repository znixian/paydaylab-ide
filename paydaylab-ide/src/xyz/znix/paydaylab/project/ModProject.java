/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.paydaylab.project;

import xyz.znix.paydaylab.data.ModFileWatcher;
import java.beans.PropertyChangeListener;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import org.netbeans.api.annotations.common.StaticResource;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectInformation;
import org.netbeans.spi.project.ProjectState;
import org.openide.filesystems.FileObject;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import xyz.znix.paydaylab.data.Definition;

/**
 *
 * @author Administrator
 */
public class ModProject implements Project {

	public static final String PROJECT_FILE = "mod.txt";

	private final FileObject projectDir;
	private final ProjectState state;
	private final FileObject definitionFile;
	private Definition definition;
	private Lookup lkp;

	ModProject(FileObject dir, ProjectState state) {
		this.projectDir = dir;
		this.state = state;
		definitionFile = projectDir.getFileObject(PROJECT_FILE);
		reloadProjectDefinition();
	}

	@Override
	public FileObject getProjectDirectory() {
		return projectDir;
	}

	public FileObject getModDefinitionFile() {
		return definitionFile;
	}

	public void reloadProjectDefinition() {
		this.definition = new Definition(definitionFile);
	}

	@Override
	public Lookup getLookup() {
		if (lkp == null) {
			lkp = Lookups.fixed(new Object[]{
				// register your features here
				this,
				new Info(),
				new ModProjectLogicalView(this),
				new ModCustomizerProvider(this),
				new ModFileWatcher(this),
				new ModActionProvider(this)
			});
		}
		return lkp;
	}

	private final class Info implements ProjectInformation {

		@StaticResource()
		public static final String MOD_ICON = "xyz/znix/paydaylab/mod_icon_16.png";

		@Override
		public Icon getIcon() {
			return new ImageIcon(ImageUtilities.loadImage(MOD_ICON));
		}

		@Override
		public String getName() {
			return getProjectDirectory().getName();
		}

		@Override
		public String getDisplayName() {
			return definition.getModName();
		}

		@Override
		public void addPropertyChangeListener(PropertyChangeListener pcl) {
			//do nothing, won't change
		}

		@Override
		public void removePropertyChangeListener(PropertyChangeListener pcl) {
			//do nothing, won't change
		}

		@Override
		public Project getProject() {
			return ModProject.this;
		}

	}
}
