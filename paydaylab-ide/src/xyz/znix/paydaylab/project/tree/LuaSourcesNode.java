/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.paydaylab.project.tree;

import java.awt.Image;
import org.netbeans.api.annotations.common.StaticResource;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

/**
 *
 * @author ZNix <znix@znix.xyz>
 */
public class LuaSourcesNode extends FilterNode {

    // TODO proper images
    @StaticResource()
    public static final String LUA_ICON_CLOSED = "xyz/znix/paydaylab/lua_icon_closed_16.png";
    @StaticResource()
    public static final String LUA_ICON_OPENED = "xyz/znix/paydaylab/lua_icon_opened_16.png";

    public LuaSourcesNode(Node folder) {
        super(folder);
    }

    @Override
    public Image getIcon(int type) {
        return ImageUtilities.loadImage(LUA_ICON_CLOSED);
    }

    @Override
    public Image getOpenedIcon(int type) {
        return ImageUtilities.loadImage(LUA_ICON_OPENED);
    }

    @NbBundle.Messages("LBL_Lua_Sources=Lua Sources")
    @Override
    public String getDisplayName() {
        return Bundle.LBL_Lua_Sources();
    }

}
