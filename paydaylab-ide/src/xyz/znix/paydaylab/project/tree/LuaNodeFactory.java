/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.paydaylab.project.tree;

import java.util.ArrayList;
import java.util.List;
import javax.swing.event.ChangeListener;
import org.netbeans.api.project.Project;
import org.netbeans.spi.project.ui.support.NodeFactory;
import org.netbeans.spi.project.ui.support.NodeList;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import xyz.znix.paydaylab.project.ModProject;

/**
 *
 * @author ZNix <znix@znix.xyz>
 */
@NodeFactory.Registration(projectType = "xyz-znix-paydaylab-mod")
public class LuaNodeFactory implements NodeFactory {

    @Override
    public NodeList<?> createNodes(Project project) {
        ModProject p = project.getLookup().lookup(ModProject.class);
        assert p != null;
        return new LuaNodeList(p);
    }

    private class LuaNodeList implements NodeList<Node> {

        ModProject project;

        public LuaNodeList(ModProject project) {
            this.project = project;
        }

        @Override
        public List<Node> keys() {
            FileObject textsFolder = project.getProjectDirectory();
            List<Node> result = new ArrayList<>();

            try {
                Node folder = DataObject.find(textsFolder).getNodeDelegate();
                result.add(new LuaSourcesNode(folder));
            } catch (DataObjectNotFoundException ex) {
                Exceptions.printStackTrace(ex);
            }

            return result;
        }

        @Override
        public Node node(Node node) {
            return new FilterNode(node);
        }

        @Override
        public void addNotify() {
        }

        @Override
        public void removeNotify() {
        }

        @Override
        public void addChangeListener(ChangeListener cl) {
        }

        @Override
        public void removeChangeListener(ChangeListener cl) {
        }

    }

}
