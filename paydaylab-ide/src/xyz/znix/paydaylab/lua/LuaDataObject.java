/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.paydaylab.lua;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.Callable;
import org.netbeans.core.api.multiview.MultiViews;
import org.netbeans.core.spi.multiview.MultiViewElement;
import org.netbeans.core.spi.multiview.text.MultiViewEditorElement;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.MIMEResolver;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.nodes.CookieSet;
import org.openide.nodes.Node.Cookie;
import org.openide.text.CloneableEditorSupport;
import org.openide.text.CloneableEditorSupport.Pane;
import org.openide.util.Lookup;
import org.openide.util.NbBundle.Messages;
import org.openide.util.lookup.InstanceContent;
import org.openide.windows.TopComponent;
import xyz.znix.paydaylab.decomp.SourceDB;
import xyz.znix.paydaylab.editor.ReadOnlyEditor;

@Messages({
    "LBL_Lua_LOADER=Files of Lua"
})
@MIMEResolver.ExtensionRegistration(
        displayName = "#LBL_Lua_LOADER",
        mimeType = "text/x-lua",
        extension = {"lua"}
)
@DataObject.Registration(
        mimeType = "text/x-lua",
        iconBase = "xyz/znix/paydaylab/lua_icon_16.png",
        displayName = "#LBL_Lua_LOADER",
        position = 300
)
@ActionReferences({
    @ActionReference(
            path = "Loaders/text/x-lua/Actions",
            id = @ActionID(category = "System", id = "org.openide.actions.OpenAction"),
            position = 100,
            separatorAfter = 200
    )
    ,
    @ActionReference(
            path = "Loaders/text/x-lua/Actions",
            id = @ActionID(category = "Edit", id = "org.openide.actions.CutAction"),
            position = 300
    )
    ,
    @ActionReference(
            path = "Loaders/text/x-lua/Actions",
            id = @ActionID(category = "Edit", id = "org.openide.actions.CopyAction"),
            position = 400,
            separatorAfter = 500
    )
    ,
    @ActionReference(
            path = "Loaders/text/x-lua/Actions",
            id = @ActionID(category = "Edit", id = "org.openide.actions.DeleteAction"),
            position = 600
    )
    ,
    @ActionReference(
            path = "Loaders/text/x-lua/Actions",
            id = @ActionID(category = "System", id = "org.openide.actions.RenameAction"),
            position = 700,
            separatorAfter = 800
    )
    ,
    @ActionReference(
            path = "Loaders/text/x-lua/Actions",
            id = @ActionID(category = "System", id = "org.openide.actions.SaveAsTemplateAction"),
            position = 900,
            separatorAfter = 1000
    )
    ,
    @ActionReference(
            path = "Loaders/text/x-lua/Actions",
            id = @ActionID(category = "System", id = "org.openide.actions.FileSystemAction"),
            position = 1100,
            separatorAfter = 1200
    )
    ,
    @ActionReference(
            path = "Loaders/text/x-lua/Actions",
            id = @ActionID(category = "System", id = "org.openide.actions.ToolsAction"),
            position = 1300
    )
    ,
    @ActionReference(
            path = "Loaders/text/x-lua/Actions",
            id = @ActionID(category = "System", id = "org.openide.actions.PropertiesAction"),
            position = 1400
    )
})
public class LuaDataObject extends MultiDataObject {

    public static String LUA_MIME = "text/x-lua";

    private CloneableEditorSupport support;
    private InstanceContent ic;
    private Lookup myLookup;
    private final boolean editable;

    public LuaDataObject(FileObject pf, MultiFileLoader loader) throws DataObjectExistsException, IOException {
        super(pf, loader);

        boolean editable = true;
        File fi = FileUtil.toFile(pf).getAbsoluteFile();
        if (fi != null) {
            File sources = SourceDB.getSourcesRoot().getAbsoluteFile();
            File dir = fi;
            while ((dir = dir.getParentFile()) != null) {
                if (sources.equals(dir)) {
                    editable = false;
                    break;
                }
            }
        }

        if (editable) {
            registerEditor(LUA_MIME, true);
        } else {
            getCookieSet().add(ReadOnlyEditor.class, new EditorFactory());
        }

        this.editable = editable;
    }

    @Override
    protected int associateLookup() {
        return 1;
    }

    private class EditorFactory implements
            CookieSet.Factory, Callable<CloneableEditorSupport.Pane> {

        @Override
        public <T extends Cookie> T createCookie(Class<T> klass) {
            if (klass.isAssignableFrom(ReadOnlyEditor.class)) {
                synchronized (this) {
                    if (support == null) {
//                    support = DataEditorSupport.create(
//                            this, getPrimaryEntry(),
//                            getCookieSet(), this
//                    );
                        support = new ReadOnlyEditor(
                                LuaDataObject.this, this
                        );
                    }
                }
                return klass.cast(support);
            }
            return null;
        }

        @Override
        public Pane call() throws Exception {
            return (Pane) MultiViews.createCloneableMultiView(LUA_MIME, LuaDataObject.this);
        }
    }

    @MultiViewElement.Registration(
            displayName = "#LBL_Lua_EDITOR",
            iconBase = "xyz/znix/paydaylab/lua_icon_16.png",
            mimeType = "text/x-lua",
            persistenceType = TopComponent.PERSISTENCE_ONLY_OPENED,
            preferredID = "lua",
            position = 1
    )
    @Messages("LBL_Lua_EDITOR=Source")
    public static MultiViewEditorElement createEditor(Lookup lkp) {
        return new MultiViewEditorElement(lkp);
    }

}
