/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.paydaylab.editor;

import javax.swing.text.Document;
import org.netbeans.api.editor.mimelookup.MimeRegistration;
import org.netbeans.api.editor.mimelookup.MimeRegistrations;
import org.netbeans.spi.editor.highlighting.HighlightsLayer;
import org.netbeans.spi.editor.highlighting.HighlightsLayerFactory;
import org.netbeans.spi.editor.highlighting.ZOrder;

/**
 *
 * @author ZNix <znix@znix.xyz>
 */
@MimeRegistrations({
    @MimeRegistration(mimeType = "text/x-lua", service = HighlightsLayerFactory.class)
})
public class MarkLuaOccurrencesHighlightsLayerFactory implements HighlightsLayerFactory {

    public static MarkLuaOccurrencesHighlighter getMarkOccurrencesHighlighter(Document doc) {
        MarkLuaOccurrencesHighlighter highlighter
                = (MarkLuaOccurrencesHighlighter) doc.getProperty(MarkLuaOccurrencesHighlighter.class);
        if (highlighter == null) {
            doc.putProperty(MarkLuaOccurrencesHighlighter.class,
                    highlighter = new MarkLuaOccurrencesHighlighter(doc));
        }
        return highlighter;
    }

    @Override
    public HighlightsLayer[] createLayers(Context context) {
        return new HighlightsLayer[]{
            HighlightsLayer.create(
            MarkLuaOccurrencesHighlighter.class.getName(),
            ZOrder.CARET_RACK.forPosition(2000),
            true,
            getMarkOccurrencesHighlighter(context.getDocument()).getHighlightsBag())
        };
    }

}
