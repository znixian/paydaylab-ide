/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.paydaylab.editor;

import java.awt.Color;
import java.io.IOException;
import java.util.concurrent.Callable;
import javax.swing.JEditorPane;
import org.openide.cookies.CloseCookie;
import org.openide.cookies.EditorCookie;
import org.openide.cookies.OpenCookie;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.text.DataEditorSupport;

/**
 *
 * @author ZNix <znix@znix.xyz>
 */
public class ReadOnlyEditor extends DataEditorSupport implements
        OpenCookie, CloseCookie, EditorCookie {

    private final Callable<Pane> factory;

    public ReadOnlyEditor(DataObject d, Callable<Pane> factory) {
        super(d, new E(d));
        this.factory = factory;
    }

    @Override
    protected Pane createPane() {
        if (factory != null) {
            try {
                return factory.call();
            } catch (Exception ex) {
                throw new IllegalStateException("Cannot create factory for " + getDataObject(), ex);
            }
        }
        return super.createPane();
    }

    private static final class E extends DataEditorSupport.Env {

        public E(DataObject d) {
            super(d);
        }

        protected FileObject getFile() {
            return getDataObject().getPrimaryFile();
        }

        protected FileLock takeLock() throws IOException {
            throw new IOException("No way!");
        }
    }
}
