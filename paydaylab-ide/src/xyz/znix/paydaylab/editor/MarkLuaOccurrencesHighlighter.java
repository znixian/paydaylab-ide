/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.paydaylab.editor;

import java.awt.Color;
import java.lang.ref.WeakReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JEditorPane;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.StyleConstants;
import net.sourceforge.luatopping.luasupport.lexer.LuaSupportTokenId;
import org.netbeans.api.editor.settings.AttributesUtilities;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.modules.editor.NbEditorUtilities;
import org.netbeans.spi.editor.highlighting.support.OffsetsBag;
import org.openide.cookies.EditorCookie;
import org.openide.loaders.DataObject;
import org.openide.util.RequestProcessor;
import xyz.znix.paydaylab.decomp.LuaClass;
import xyz.znix.paydaylab.decomp.SourceDB;

/**
 *
 * @author ZNix <znix@znix.xyz>
 */
public class MarkLuaOccurrencesHighlighter implements CaretListener {

    private static final AttributeSet defaultColors = AttributesUtilities.createImmutable(
            StyleConstants.Background, new Color(236, 235, 163)
    );

    private final OffsetsBag bag;

    private JTextComponent comp;
    private final WeakReference<Document> weakDoc;

    private final RequestProcessor rp;
    private final static int REFRESH_DELAY = 500;
    private RequestProcessor.Task lastRefreshTask;

    public MarkLuaOccurrencesHighlighter(Document doc) {
        rp = new RequestProcessor(MarkLuaOccurrencesHighlighter.class);
        bag = new OffsetsBag(doc);
        weakDoc = new WeakReference<>(doc);
        DataObject dobj = NbEditorUtilities.getDataObject(weakDoc.get());
        if (dobj != null) {
            EditorCookie pane = dobj.getLookup().lookup(EditorCookie.class);
            JEditorPane[] panes = pane.getOpenedPanes();
            if (panes != null && panes.length > 0) {
                comp = panes[0];
                comp.addCaretListener(this);
            }
        }
    }

    @Override
    public void caretUpdate(CaretEvent e) {
        bag.clear();
        setupAutoRefresh();
    }

    public void setupAutoRefresh() {
        if (lastRefreshTask == null) {
            lastRefreshTask = rp.create(this::updateSelection);
        }
        lastRefreshTask.schedule(REFRESH_DELAY);
    }

    private void updateSelection() {
		// TODO check the editor is still open
        String selection = comp.getSelectedText();
        if (selection != null) {
            highlightAll(selection.replace("\r\n", "\n"));
        } else {
            // highlightNextToken();
        }
    }

    private void highlightAll(String pttn) {
        Pattern p = Pattern.compile(pttn);
        Matcher m = p.matcher(comp.getText().replace("\r\n", "\n"));
        while (m.find() == true) {
            int startOffset = m.start();
            int endOffset = m.end();
            bag.addHighlight(startOffset, endOffset, defaultColors);
        }
    }

    private void highlightNextToken() {
        AbstractDocument doc = (AbstractDocument) comp.getDocument();
        doc.readLock();
        try {
            TokenHierarchy hi = TokenHierarchy.get(doc);
            TokenSequence<LuaSupportTokenId> ts
                    = hi.tokenSequence(LuaSupportTokenId.getLanguage());
            if (ts != null) {
                ts.move(comp.getCaretPosition());
                ts.moveNext();
                Token<LuaSupportTokenId> tok = ts.token();
                int newOffset = ts.offset();
                String matcherText = tok.text().toString();

                SourceDB db = SourceDB.get(false);

                // Still indexing
                if (db == null) {
                    return;
                }

                LuaClass luaClass = db.getLuaClass(matcherText);
                if (luaClass == null) {
                    return;
                }

                highlightAll(matcherText);
            }
        } finally {
            doc.readUnlock();
        }
    }

    public OffsetsBag getHighlightsBag() {
        return bag;
    }

}
