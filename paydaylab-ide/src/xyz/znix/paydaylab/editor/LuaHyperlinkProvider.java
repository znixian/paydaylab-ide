/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.paydaylab.editor;

import java.io.File;
import javax.swing.text.AbstractDocument;
import javax.swing.text.Document;
import net.sourceforge.luatopping.luasupport.lexer.LuaSupportTokenId;
import org.netbeans.api.editor.mimelookup.MimeRegistration;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.lib.editor.hyperlink.spi.HyperlinkProvider;
import org.openide.cookies.LineCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.text.Line;
import org.openide.util.Exceptions;
import xyz.znix.paydaylab.decomp.LuaClass;
import xyz.znix.paydaylab.decomp.SourceDB;

/**
 *
 * @author ZNix <znix@znix.xyz>
 */
@MimeRegistration(mimeType = "text/x-lua", service = HyperlinkProvider.class)
public class LuaHyperlinkProvider implements HyperlinkProvider {

    private LuaClass target;
    private int targetStart;
    private int targetEnd;

    @Override
    public boolean isHyperlinkPoint(Document doc, int offset) {
        return verifyState(doc, offset);
    }

    public boolean verifyState(Document doc, int offset) {
        // TODO is this safe?
        AbstractDocument adoc = (AbstractDocument) doc;
        adoc.readLock();
        try {
            TokenHierarchy hi = TokenHierarchy.get(doc);
            TokenSequence<LuaSupportTokenId> ts
                    = hi.tokenSequence(LuaSupportTokenId.getLanguage());
            if (ts != null) {
                ts.move(offset);
                ts.moveNext();
                Token<LuaSupportTokenId> tok = ts.token();
                int newOffset = ts.offset();
                String matcherText = tok.text().toString();

                SourceDB db = SourceDB.get(false);

                // Still indexing
                if (db == null) {
                    return false;
                }

                LuaClass luaClass = db.getLuaClass(matcherText);
                if (luaClass != null) {
                    target = luaClass;
                    int idx = matcherText.indexOf(luaClass.getName());
                    targetStart = newOffset + idx;
                    targetEnd = targetStart + luaClass.getName().length();
                    return true;
                }
            }
            return false;
        } finally {
            adoc.readUnlock();
        }
    }

    @Override
    public int[] getHyperlinkSpan(Document document, int offset) {
        if (verifyState(document, offset)) {
            return new int[]{targetStart, targetEnd};
        } else {
            return null;
        }
    }

    @Override
    public void performClickAction(Document document, int offset) {
        if (verifyState(document, offset)) {
            File f = target.getSource();
            FileObject fo = FileUtil.toFileObject(f);
            try {
                DataObject dobj = DataObject.find(fo);
                LineCookie lc = dobj.getLookup().lookup(LineCookie.class);
                if (lc == null) {
                    return;
                }

                Line l = lc.getLineSet().getOriginal(10);
                l.show(Line.ShowOpenType.OPEN, Line.ShowVisibilityType.FOCUS);
            } catch (DataObjectNotFoundException ex) {
                Exceptions.printStackTrace(ex);
            }
        }
    }
}
