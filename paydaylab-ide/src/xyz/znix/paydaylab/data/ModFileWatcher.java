/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.paydaylab.data;

import org.netbeans.spi.project.ui.ProjectOpenedHook;
import org.openide.filesystems.FileAttributeEvent;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileRenameEvent;
import xyz.znix.paydaylab.project.ModProject;

/**
 *
 * @author ZNix <znix@znix.xyz>
 */
public class ModFileWatcher extends ProjectOpenedHook implements FileChangeListener {

    private final ModProject mod;

    public ModFileWatcher(ModProject mod) {
        this.mod = mod;
    }

    @Override
    protected void projectOpened() {
        mod.getModDefinitionFile().addFileChangeListener(this);
    }

    @Override
    protected void projectClosed() {
        mod.getModDefinitionFile().removeFileChangeListener(this);
    }

    @Override
    public void fileChanged(FileEvent fe) {
        mod.reloadProjectDefinition();
    }

    @Override
    public void fileFolderCreated(FileEvent fe) {
    }

    @Override
    public void fileDataCreated(FileEvent fe) {
    }

    @Override
    public void fileDeleted(FileEvent fe) {
    }

    @Override
    public void fileRenamed(FileRenameEvent fe) {
    }

    @Override
    public void fileAttributeChanged(FileAttributeEvent fe) {
    }
}
