/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.paydaylab.run;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Represents a single sendable debugging message. Each message keeps track of
 * if it has yet been sent, and can be waited upon until it has been sent.
 *
 * @author ZNix <znix@znix.xyz>
 */
public class SendableMessage {

	public static final int TYPE_LOG = 1;
	public static final int TYPE_INIT = 2;
	private static final int CTYPE_CONTINUE = 3;
	private static final int CTYPE_START_VR = 4;
	private static final int CTYPE_LUA_MESSAGE = 5;
	/** A Lua message sent from the client */
	public static final int TYPE_LUA_MESSAGE = 6;

	private final ByteBuffer data;
	private final AtomicBoolean sent;

	public static SendableMessage makeContinue() {
		ByteBuffer msg = ByteBuffer.allocate(4);
		msg.putInt(CTYPE_CONTINUE);
		return new SendableMessage(msg);
	}

	public static SendableMessage makeStartVR() {
		ByteBuffer msg = ByteBuffer.allocate(4);
		msg.putInt(CTYPE_START_VR);
		return new SendableMessage(msg);
	}

	public static SendableMessage makeLuaMessage(String payload) {
		// Get the byte representation of the string
		byte[] data = payload.getBytes();

		// 4 bytes for the ID, 4 bytes for the length, the size of the data
		ByteBuffer msg = ByteBuffer.allocate(4 + 4 + data.length);
		msg.putInt(CTYPE_LUA_MESSAGE);
		msg.putInt(data.length);
		msg.put(data);
		return new SendableMessage(msg);
	}

	public SendableMessage(ByteBuffer data) {
		this.data = data;
		sent = new AtomicBoolean(false);
	}

	public void markSent() {
		synchronized (sent) {
			sent.set(true);
			sent.notifyAll();
		}
	}

	public void waitUntilSent() throws InterruptedException {
		synchronized (sent) {
			while (!sent.get()) {
				sent.wait();
			}
		}
	}

	public ByteBuffer getData() {
		return data;
	}

	public boolean isSent() {
		// I don't *think* we have to synchronize this?
		return sent.get();
	}

}
