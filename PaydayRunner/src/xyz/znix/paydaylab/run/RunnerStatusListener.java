/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.paydaylab.run;

/**
 *
 * @author ZNix
 */
public interface RunnerStatusListener {

	void messageReceived(String message);

	void taskFinished();
}
