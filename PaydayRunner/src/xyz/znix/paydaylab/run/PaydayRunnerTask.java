/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.paydaylab.run;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.prefs.Preferences;
import org.netbeans.api.debugger.DebuggerInfo;
import org.netbeans.api.debugger.DebuggerManager;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.spi.debugger.SessionProvider;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;
import org.openide.util.RequestProcessor;
import org.openide.windows.IOProvider;
import org.openide.windows.InputOutput;
import xyz.znix.paydaylab.options.PaydayPanel;

/**
 * A task to run an instance of PAYDAY 2, and to maintain a debugging socket
 * connection to it.
 *
 * @author ZNix <znix@znix.xyz>
 */
public class PaydayRunnerTask {

	public static final String PAYDAY_DEBUGGER_INFO = "PaydayDebuggerInfo";
	public static final String PAYDAY_SESSION = "PaydaySession";

	private final static RequestProcessor REQ_PROC
			= new RequestProcessor("pd2 runner", 1, true);

	private static final String DEBUG_OPT = "--debug-params";

	private static PaydayRunnerTask runningTask;
	private static RunnerStatusListener statusCallback;

	private final boolean debugging_mode;
	private final RequestProcessor.Task nbTask;
	private final BlockingQueue<SendableMessage> messages;
	private ServerSocket server;
	private Socket client;
	private OutputStream outputStream;

	private static InputOutput lastIO;

	public static boolean isRunning() {
		return runningTask != null;
	}

	public static SendableMessage sendLuaMessage(String payload) {
		if (runningTask == null) {
			throw new IllegalStateException("PS2 Run Task Not Running!");
		}

		SendableMessage msg = SendableMessage.makeLuaMessage(payload);
		runningTask.messages.add(msg);

		return msg;
	}

	public static void setStatusCallback(RunnerStatusListener callback) {
		statusCallback = callback;
	}

	public static void startDebug() {
		SessionProvider session = new SessionProvider() {

			@Override
			public String getSessionName() {
				return "Payday Program";
			}

			@Override
			public String getLocationName() {
				return "localhost";
			}

			@Override
			public String getTypeID() {
				return PAYDAY_SESSION;
			}

			@Override
			public Object[] getServices() {
				return new Object[]{};
			}
		};

		DebuggerInfo info = DebuggerInfo.create(PAYDAY_DEBUGGER_INFO,
				new Object[]{session, null});

		DebuggerManager manager = DebuggerManager.getDebuggerManager();
		manager.startDebugging(info);
	}

	@NbBundle.Messages("LBL_Payday_Run=Run PAYDAY 2")
	public static void start(boolean debug) {
		PaydayRunnerTask runner = new PaydayRunnerTask(debug);

		final ProgressHandle ph = ProgressHandle.createHandle(
				Bundle.LBL_Payday_Run(), runner.nbTask);

		runner.nbTask.addTaskListener(finished -> {
			//make sure that we get rid of the ProgressHandle
			//when the task is finished
			ph.finish();
		});

		// start the progresshandle the progress UI will show 500s after
		ph.start();

		//this actually start the task
		runner.nbTask.schedule(0);
	}

	private PaydayRunnerTask(boolean debugging_mode) {
		messages = new LinkedBlockingQueue<>();
		nbTask = REQ_PROC.create(this::taskedInterrupt);
		this.debugging_mode = debugging_mode;
	}

	private void taskedInterrupt() {
		runningTask = this;

		Thread mainRunner = new Thread(this::run, "PaydayNetworkConnector");
		mainRunner.start();

		Thread writer = new Thread(this::writeLoop, "PaydayNetworkWriter");
		writer.start();

		// Wait until the main runner is done (client disconnects), or we're interrupted.
		try {
			mainRunner.join();
		} catch (InterruptedException ix) {
			// Someone cancelled this thread
			// Pull the plugs out, which does the trick of stopping it.
			// This should also interrupt the writer thread, if it's currently
			//   trying to send a message.

			try {
				if (server != null) {
					server.close();
				}
				if (client != null) {
					client.close();
				}
			} catch (IOException ex) {
				Exceptions.printStackTrace(ex);
			}
		}

		// Now close the writer
		writer.interrupt();

		if (statusCallback != null) {
			statusCallback.taskFinished();
		}

		runningTask = null;
	}

	public static void interrupt() {
		runningTask.nbTask.cancel();
	}

	private void writeLoop() {
		try {
			List<SendableMessage> toNotifyFlushed = new ArrayList<>();
			while (true) {
				SendableMessage message = messages.take();
				outputStream.write(message.getData().array());
				toNotifyFlushed.add(message);

				// If we're not about to write any more messages,
				// flush what we just write.
				if (messages.isEmpty()) {
					outputStream.flush();

					// Notify all the messages as flushed
					toNotifyFlushed.forEach(SendableMessage::markSent);
					toNotifyFlushed.clear();
				}
			}
		} catch (InterruptedException ex) {
			// Client probably closed, we're done.
		} catch (IOException ex) {
			// This shouldn't be happening.
			Exceptions.printStackTrace(ex);
		}
	}

	@NbBundle.Messages("LBL_Payday=PAYDAY 2")
	private void run() {
		Preferences prefs = NbPreferences.forModule(PaydayPanel.class);
		String runCmd = prefs.get("steamExecPath",
				PaydayPanel.DEFAULT_EXEC_CMD).trim();

//		if (!new File(steamExec).exists()) {
//			JOptionPane.showMessageDialog(null, "The Steam executable is missing."
//					+ "\nPlease check it's path in the options menu.",
//					"Missing Steam Exec", JOptionPane.ERROR_MESSAGE);
//			return;
//		}
		InputOutput io = IOProvider.getDefault().getIO(Bundle.LBL_Payday(), false);

		// Make sure we don't leave old outputs behind
		// FIXME surely there's a cleaner way to do this?
		if (io == lastIO) {
			io.closeInputOutput();
			io = IOProvider.getDefault().getIO(Bundle.LBL_Payday(), true);
		}

		lastIO = io;
		io.select();

		try {
			server = new ServerSocket(0);

			String key = UUID.randomUUID().toString();
			String params = "127.0.0.1:" + server.getLocalPort() + ":" + key;

			if (!prefs.getBoolean("enableVR", false)) {
				List<String> args = new ArrayList();
				args.addAll(Arrays.asList(runCmd.split("`")));
				args.addAll(Arrays.asList(
						DEBUG_OPT, params
				));

				if (debugging_mode) {
					args.add("--debug-lua-param");
					args.add("debug_enable");
					args.add("true");
				}

				ProcessBuilder pb = new ProcessBuilder(args);
				Process steam = pb.start();

				io.getErr().println("Starting game with params " + params);
			} else {
				// TODO add setting for this
				String pd2dir = "C:\\Program Files (x86)\\Steam\\steamapps\\common\\PAYDAY 2";

				File debug = new File(pd2dir, "remote-debug.conf");
				Files.write(debug.toPath(), params.getBytes());
				io.getErr().println("Waiting for gamelaunch on " + params);
			}

			// Game should start, now wait for the connection
			client = server.accept();

			handleClient(io, key);

			io.getErr().println("Debugging connection closed");
		} catch (IOException ex) {
			ex.printStackTrace(io.getErr());
		} catch (InterruptedException ex) {
			// Quit out, no problem here
		} finally {
			// TODO some way to close it without closing the physical window
			io.getOut().close();
			io.getErr().close();

			try {
				if (server != null) {
					server.close();
				}
				if (client != null) {
					client.close();
				}
			} catch (IOException ex) {
				Exceptions.printStackTrace(ex);
			}
		}
	}

	private void handleClient(InputOutput io, String key)
			throws IOException, InterruptedException {

		try {
			// Note everything is written big-endian
			DataInputStream in = new DataInputStream(
					new BufferedInputStream(client.getInputStream()));
			outputStream = new BufferedOutputStream(client.getOutputStream());

			int b;
			boolean authenticated = false;
			while (true) {
				b = in.readInt();
				if (b == -1) {
					break;
				}

				if (b == SendableMessage.TYPE_INIT) {
					int chars = in.readInt();
					char[] input = new char[chars];
					for (int i = 0; i < chars; i++) {
						// TODO fix UTF-8 support
						input[i] = (char) in.readByte();
					}

					String respkey = String.valueOf(input);
					if (!key.equals(respkey)) {
						io.getErr().println("Invalid authkey: " + respkey);
						return;
					} else {
						io.getErr().println("Client successfully authenticated.");
						authenticated = true;

						messages.add(SendableMessage.makeContinue());

						continue;
					}
				}

				if (!authenticated) {
					io.getErr().println("Client tried to send message before authenticating!");
					return;
				}

				if (b == SendableMessage.TYPE_LOG) {
					int chars = in.readInt();
					char[] input = new char[chars];
					for (int i = 0; i < chars; i++) {
						// TODO fix UTF-8 support
						input[i] = (char) in.readByte();
					}

					String message = String.valueOf(input);
					io.getOut().println(message);
				} else if (b == SendableMessage.TYPE_LUA_MESSAGE) {
					int chars = in.readInt();
					char[] input = new char[chars];
					for (int i = 0; i < chars; i++) {
						// TODO fix UTF-8 support
						input[i] = (char) in.readByte();
					}

					String message = String.valueOf(input);
					if (statusCallback != null) {
						statusCallback.messageReceived(message);
					}
				}
			}
		} finally {
			if (!client.isClosed()) {
				client.close();
			}

			// TODO do we need to close in and out?
		}
	}
}
