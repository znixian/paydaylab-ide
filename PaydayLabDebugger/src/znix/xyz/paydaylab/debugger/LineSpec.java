/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package znix.xyz.paydaylab.debugger;

import org.openide.text.Line;

/**
 * A representation of a line of code
 *
 * @author ZNix
 */
public interface LineSpec {

	Line getEditorLine();
}
