/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package znix.xyz.paydaylab.debugger;

import org.netbeans.api.debugger.DebuggerEngine;
import org.netbeans.spi.debugger.DebuggerEngineProvider;

/**
 *
 * @author ZNix
 */
public class PaydayEngineProvider extends DebuggerEngineProvider {

	private DebuggerEngine.Destructor destructor;
	
	public PaydayEngineProvider() {
		System.out.println("znix.xyz.paydaylab.debugger.PaydayEngineProvider.<init>()");
	}

	@Override
	public String[] getLanguages() {
		return new String[]{"Lua"};
	}

	@Override
	public String getEngineTypeID() {
		return "PaydayDebuggerEngine";
	}

	@Override
	public Object[] getServices() {
		return new Object[]{};
	}

	@Override
	public void setDestructor(DebuggerEngine.Destructor destructor) {
		this.destructor = destructor;
	}

	public DebuggerEngine.Destructor getDestructor() {
		return destructor;
	}

}
