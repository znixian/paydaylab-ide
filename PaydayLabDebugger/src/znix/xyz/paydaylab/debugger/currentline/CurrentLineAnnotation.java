/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package znix.xyz.paydaylab.debugger.currentline;

import org.openide.text.Annotation;

/**
 * A annotation that highlights the currently executing line.
 *
 * @author ZNix
 */
public class CurrentLineAnnotation extends Annotation {

	public CurrentLineAnnotation() {
	}

	@Override
	public String getAnnotationType() {
		// This annotation type is defined internally by netbeans
		// so the current line indicators are consistent between languages.
		return "CurrentPC";
	}

	@Override
	public String getShortDescription() {
		return "Current Line";
	}
}
