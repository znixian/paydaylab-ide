/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package znix.xyz.paydaylab.debugger.currentline;

import java.awt.EventQueue;
import java.io.File;
import java.util.regex.*;
import org.openide.cookies.LineCookie;
import org.openide.filesystems.*;
import org.openide.loaders.*;
import org.openide.text.Line;
import znix.xyz.paydaylab.debugger.LineSpec;

/**
 *
 * @author ZNix
 */
public class CurrentLineManager {

	private static final Pattern MOD_PATTERN = Pattern.compile("@mods/([^/]+)/(.*)");

	private final CurrentLineAnnotation annotation;

	public CurrentLineManager() {
		annotation = new CurrentLineAnnotation();
	}

	public void clearLine() {
		annotation.detach();
	}

	public void setLine(LineSpec lineSpec) {
		clearLine();

		Line l = lineSpec.getEditorLine();

		EventQueue.invokeLater(() -> l.show(Line.ShowOpenType.OPEN,
				Line.ShowVisibilityType.FOCUS));

		annotation.attach(l);
	}

	public static Line getLineFromDescription(String fileStr, int line) {
		FileObject file;
		Matcher matcher = MOD_PATTERN.matcher(fileStr);
		if (matcher.matches()) {
			file = getFileFromString(matcher);
		} else {
			file = null; // TODO
		}

		if (file == null) {
			// TODO indicate this somehow
			return null;
		}

		// Open the editor
		DataObject dobj;
		try {
			dobj = DataObject.find(file);
		} catch (DataObjectNotFoundException ex) {
			// TODO should we log this?
			return null;
		}

		if (dobj == null) {
			return null;
		}

		LineCookie lc = (LineCookie) dobj.getCookie(LineCookie.class);
		if (lc == null) {
			// cannot do it
			return null;
		}

		// -1 because NetBeans starts lines a 0, and Lua starts them at 1
		return lc.getLineSet().getOriginal(line - 1);
	}

	private static FileObject getFileFromString(Matcher input) {
		String mod = input.group(1);
		String name = input.group(2);

		// TODO move this into settings
		String PD2_MODS_PATH = "C:\\Users\\ZNix\\Static\\steamcmd\\steamapps\\common\\PAYDAY 2\\mods";
		FileObject mods = FileUtil.toFileObject(new File(PD2_MODS_PATH));

		for (FileObject modDir : mods.getChildren()) {
			if (!modDir.getNameExt().matches(mod)) {
				continue;
			}

			FileObject file = modDir.getFileObject(name);

			return file;
		}

		return null;
	}
}
