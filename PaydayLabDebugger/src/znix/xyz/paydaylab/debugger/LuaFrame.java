/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package znix.xyz.paydaylab.debugger;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.json.simple.JSONObject;
import org.openide.text.Line;
import znix.xyz.paydaylab.debugger.currentline.CurrentLineManager;
import znix.xyz.paydaylab.debugger.locals.Var;

/**
 * An immutable representation of a Lua stackframe.
 *
 * @author ZNix
 */
public class LuaFrame implements LineSpec {

	private final LuaFrame parent;
	private final String source;
	private final int currentLine;
	private final Map<String, Var> variables;
	private final Map<String, Var> upvalues;

	public LuaFrame(LuaFrame parent, JSONObject json) {
		this.parent = parent;

		source = (String) json.get("source");
		currentLine = ((Number) json.get("current-line")).intValue();

		// TODO
		variables = new HashMap<>();
		upvalues = new HashMap<>();

		Object serializedVariables = (Object) json.get("variables");
		// If there are no variables, it will be an empty array
		if (serializedVariables instanceof JSONObject) {
			((JSONObject) serializedVariables).forEach((k, v) -> {
				variables.put(k.toString(), Var.fromJSON((JSONObject) v));
			});
		}

		Object serializedUpvalues = (Object) json.get("upvalues");
		if (serializedUpvalues instanceof JSONObject) {
			((JSONObject) serializedUpvalues).forEach((k, v) -> {
				upvalues.put(k.toString(), Var.fromJSON((JSONObject) v));
			});
		}
	}

	public Map<String, Var> getVariables() {
		return Collections.unmodifiableMap(variables);
	}

	public Map<String, Var> getUpvalues() {
		return Collections.unmodifiableMap(upvalues);
	}

	public LuaFrame getParent() {
		return parent;
	}

	public String getSource() {
		return source;
	}

	public int getCurrentLine() {
		return currentLine;
	}

	boolean matches(LuaFrame prev, JSONObject frame) {
		if (prev != parent) {
			return false;
		}

		if (!source.equals(frame.get("source"))) {
			return false;
		}

		if (currentLine != ((Number) frame.get("current-line")).intValue()) {
			return false;
		}

		return true;
	}

	@Override
	public Line getEditorLine() {
		return CurrentLineManager.getLineFromDescription(source, currentLine);
	}
}
