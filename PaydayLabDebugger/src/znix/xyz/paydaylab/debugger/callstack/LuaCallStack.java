/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package znix.xyz.paydaylab.debugger.callstack;

import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Action;
import org.netbeans.spi.debugger.ContextProvider;
import org.netbeans.spi.viewmodel.*;
import org.openide.text.Line;
import znix.xyz.paydaylab.debugger.DebugSession;
import znix.xyz.paydaylab.debugger.LuaFrame;
import znix.xyz.paydaylab.debugger.PaydayDebugger;

/**
 *
 *
 * @author ZNix
 */
public class LuaCallStack implements TreeModel, NodeActionsProvider,
		TableModel, NodeModel {

	private final List<ModelListener> listeners;
	private final DebugSession session;

	public static final String CALL_STACK
			= "org/netbeans/modules/debugger/resources/callStackView/NonCurrentFrame";
	public static final String CURRENT_CALL_STACK
			= "org/netbeans/modules/debugger/resources/callStackView/CurrentFrame";

	public LuaCallStack(ContextProvider contextProvider) {
		// TODO do we need to use a thread-safe wrapper?
		listeners = new ArrayList<>();
		PaydayDebugger debugger = PaydayDebugger.getDebugger(contextProvider);
		debugger.setCallStack(this);
		session = debugger.getSession();
	}

	// TreeModel
	@Override
	public Object getRoot() {
		return ROOT;
	}

	@Override
	public Object[] getChildren(Object parent, int from, int to)
			throws UnknownTypeException {
		if (parent == ROOT) {
			if (session.isRunning()) {
				return new Object[0];
			}
			Object[] frames = session.getStackFramesArray();

			// Flip the stacks around, so the last call appears on top
			for (int i = 0; i < frames.length / 2; i++) {
				Object f = frames[i];
				frames[i] = frames[frames.length - i - 1];
				frames[frames.length - i - 1] = f;
			}

			return frames;
		} else {
			throw new UnknownTypeException(parent);
		}
	}

	@Override
	public boolean isLeaf(Object node) throws UnknownTypeException {
		if (node == ROOT) {
			return false;
		} else if (node instanceof LuaFrame) {
			return true;
		} else {
			throw new UnknownTypeException(node);
		}
	}

	@Override
	public int getChildrenCount(Object node) throws UnknownTypeException {
		if (node == ROOT) {
			if (session.isRunning()) {
				return 0;
			}
			return session.getStackFrameCount();
		} else {
			throw new UnknownTypeException(node);
		}
	}

	@Override
	public void addModelListener(ModelListener l) {
		listeners.add(l);
	}

	@Override
	public void removeModelListener(ModelListener l) {
		listeners.remove(l);
	}

	public void fireChanges() {
		listeners.forEach(l -> l.modelChanged(new ModelEvent.TreeChanged(this)));
	}

	// NodeModel
	@Override
	public String getDisplayName(Object node) throws UnknownTypeException {
		if (node == ROOT) {
			return "Callstack"; // TODO i18n
		} else if (node instanceof LuaFrame) {
			LuaFrame frame = (LuaFrame) node;
			return frame.getSource() + ":" + frame.getCurrentLine();
		} else {
			throw new UnknownTypeException(node);
		}
	}

	@Override
	public String getIconBase(Object node) throws UnknownTypeException {
		if (node instanceof LuaFrame) {
			return CALL_STACK;
		} else if (node == ROOT) {
			return null;
		} else {
			throw new UnknownTypeException(node);
		}
	}

	@Override
	public String getShortDescription(Object node) throws UnknownTypeException {
		if (node instanceof LuaFrame) {
			return ((LuaFrame) node).toString();
		} else if (node == ROOT) {
			return "Callstack"; // TODO i18n
		} else {
			throw new UnknownTypeException(node);
		}
	}

	// NodeActionProvider
	@Override
	public void performDefaultAction(Object node) throws UnknownTypeException {
		if (node instanceof LuaFrame) {
			// Handle this below
		} else if (node == ROOT) {
			// Don't do anything
			return;
		} else {
			throw new UnknownTypeException(node);
		}

		LuaFrame frame = (LuaFrame) node;
		Line line = frame.getEditorLine();
		if (line == null) {
			return;
		}
		EventQueue.invokeLater(() -> line.show(Line.ShowOpenType.OPEN,
				Line.ShowVisibilityType.FOCUS));
	}

	@Override
	public Action[] getActions(Object o) throws UnknownTypeException {
		return new Action[]{};
	}

	// TableModel
	@Override
	public Object getValueAt(Object o, String string) throws UnknownTypeException {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public boolean isReadOnly(Object o, String string) throws UnknownTypeException {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void setValueAt(Object o, String string, Object o1) throws UnknownTypeException {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}
}
