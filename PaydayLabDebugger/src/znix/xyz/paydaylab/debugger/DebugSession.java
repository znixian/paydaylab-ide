/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package znix.xyz.paydaylab.debugger;

import java.util.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author ZNix
 */
public class DebugSession {

	private final List<LuaFrame> stack;
	private final LuaHeap heap;
	private boolean running = true;

	public DebugSession() {
		stack = new ArrayList<>();
		heap = new LuaHeap();
	}

	public void update(JSONObject data) {
		running = !(Boolean) data.get("enabled");

		if (!running) {
			updateStackFrames((JSONArray) data.get("stack"));
		} else {
			heap.clear();
		}
	}

	public LuaHeap getHeap() {
		checkRunning();

		return heap;
	}

	public LuaFrame getStackFrame(int index) {
		checkRunning();

		if (index < 0) {
			// So -1 becomes the end of the stack, -2 becomes one before
			// kinda like in the C interface for Lua
			index += stack.size();
		}

		return stack.get(index);
	}

	public int getStackFrameCount() {
		checkRunning();
		return stack.size();
	}

	public LuaFrame[] getStackFramesArray() {
		checkRunning();
		return stack.toArray(new LuaFrame[stack.size()]);
	}

	public boolean isRunning() {
		return running;
	}

	private void checkRunning() {
		if (running) {
			throw new IllegalStateException("Cannot get stack frame of running session!");
		}
	}

	private void updateStackFrames(JSONArray frames) {
		LuaFrame prev = null;
		for (int i = 0; i < frames.size(); i++) {
			JSONObject frame = (JSONObject) frames.get(i);

			LuaFrame finalFrame = null;
			if (stack.size() > i) {
				finalFrame = stack.get(i);

				if (!finalFrame.matches(prev, frame)) {
					finalFrame = null;
				}
			}

			if (finalFrame == null) {
				finalFrame = new LuaFrame(prev, frame);
				if (stack.size() > i) {
					stack.set(i, finalFrame);
				} else {
					stack.add(finalFrame);
				}
			}
			
			prev = finalFrame;
		}
	}
}
