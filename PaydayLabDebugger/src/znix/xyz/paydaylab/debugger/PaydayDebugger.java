/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package znix.xyz.paydaylab.debugger;

import java.awt.EventQueue;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.logging.*;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.netbeans.spi.debugger.*;
import xyz.znix.paydaylab.run.PaydayRunnerTask;
import static org.netbeans.api.debugger.ActionsManager.*;
import org.netbeans.api.debugger.DebuggerManager;
import znix.xyz.paydaylab.debugger.breakpoints.RemoteBreakpointManager;
import znix.xyz.paydaylab.debugger.currentline.CurrentLineManager;
import xyz.znix.paydaylab.run.RunnerStatusListener;
import znix.xyz.paydaylab.debugger.callstack.LuaCallStack;
import znix.xyz.paydaylab.debugger.locals.LocalsView;

/**
 *
 * @author ZNix
 */
public class PaydayDebugger extends ActionsProviderSupport implements RunnerStatusListener {

	private static final Set<Object> ACTIONS;
	private final PaydayEngineProvider engineProvider;
	private final CurrentLineManager currentLine;
	private final DebugSession session;
	private LuaCallStack callStack;
	private LocalsView localsView;

	// TODO remove this, and use lookups instead
	private static PaydayDebugger INSTANCE;

	/**
	 * Make an array of actions for convenience.
	 */
	static {
		Set<Object> actions = new HashSet<>();

		// Handled here
		actions.add(ACTION_KILL);
		actions.add(ACTION_START);

		// Handled by Lua
		// In string form, these are pause, stepInto, etc
		actions.add(ACTION_PAUSE);
		actions.add(ACTION_CONTINUE);
		actions.add(ACTION_STEP_INTO);
		actions.add(ACTION_STEP_OVER);
		actions.add(ACTION_RUN_TO_CURSOR);

		ACTIONS = Collections.unmodifiableSet(actions);
	}

	public PaydayDebugger(ContextProvider contextProvider) {
		//if we not enable the actions, our debugger will show them as greyed
		//out by default, in both the menus and the toolbar.
		engineProvider = (PaydayEngineProvider) contextProvider.lookupFirst(null, DebuggerEngineProvider.class);
		for (Iterator it = ACTIONS.iterator(); it.hasNext();) {
			setEnabled(it.next(), true);
		}

		currentLine = new CurrentLineManager();

		session = new DebugSession();
	}

	public static PaydayDebugger getDebugger(ContextProvider context) {
		return INSTANCE;
	}

	/**
	 * This is where we implement (or delegate), the implementation of our
	 * debugger. In other words, this is where we tell our debugger
	 * implementation to step over, into, stop, or to take other custom
	 * operations.
	 *
	 * @param action
	 */
	@Override
	public void doAction(Object action) {
		if (action == ACTION_KILL) {
			// this stops the debugger
			engineProvider.getDestructor().killEngine();

			// Reset the callback first, so this action isn't run when it exits.
			PaydayRunnerTask.setStatusCallback(null);

			// Cancel it
			PaydayRunnerTask.interrupt();

			// Remove the current line marker, if it exists
			currentLine.clearLine();

			INSTANCE = null;
		} else if (action == ACTION_START) {
			PaydayRunnerTask.start(true);
			PaydayRunnerTask.setStatusCallback(this);
			INSTANCE = this;
		} else {
			final JSONObject message = new JSONObject();

			message.put("op", "ide-action");
			message.put("action", action.toString());

			if (action == ACTION_RUN_TO_CURSOR) {
				try {
					EventQueue.invokeAndWait(() -> {
						message.put("selected-line", DebuggerUtils.getBreakpointAtLine());
					});
				} catch (InterruptedException | InvocationTargetException ex) {
					throw new RuntimeException(ex);
				}
			}

			PaydayRunnerTask.sendLuaMessage(message.toJSONString());
		}
	}

	@Override
	public Set getActions() {
		return ACTIONS;
	}

	@Override
	public void messageReceived(String message) {
		JSONParser parser = new JSONParser();
		try {
			JSONObject data = (JSONObject) parser.parse(message);
			if (data.get("op").equals("get-breakpoints")) {
				RemoteBreakpointManager.UpdateBreakpoints();
			} else if (data.get("op").equals("session")) {
				session.update(data);
				if (callStack != null) {
					callStack.fireChanges();
				}
				if (localsView != null) {
					localsView.fireChanges();
				}
				if (session.isRunning()) {
					currentLine.clearLine();
				} else {
					currentLine.setLine(session.getStackFrame(-1));
				}
			} else if (data.get("op").equals("heap-value")) {
				session.getHeap().apply(data, localsView::fireChangedTable);
			}
		} catch (ParseException ex) {
			Logger.getLogger(PaydayDebugger.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	@Override
	public void taskFinished() {
		DebuggerManager.getDebuggerManager().getCurrentEngine()
				.getActionsManager().doAction(ACTION_KILL);
	}

	public DebugSession getSession() {
		return session;
	}

	public void setCallStack(LuaCallStack callStack) {
		this.callStack = callStack;
	}

	public void setLocalsView(LocalsView localsView) {
		this.localsView = localsView;
	}
}
