/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package znix.xyz.paydaylab.debugger;

import java.util.*;
import java.util.function.Consumer;
import org.json.simple.JSONObject;
import xyz.znix.paydaylab.run.PaydayRunnerTask;
import znix.xyz.paydaylab.debugger.locals.*;

/**
 * Represents the state of the Lua Heap
 *
 * @author ZNix
 */
public class LuaHeap {

	private final Map<String, VarTable> tables;
	private final Set<String> requested;

	public LuaHeap() {
		tables = new HashMap<>();
		requested = new HashSet<>();
	}

	public VarTable get(String key) {
		return tables.get(key);
	}

	public VarTable getOrRequest(String key) {
		Objects.requireNonNull(key);

		if (!tables.containsKey(key) && !requested.contains(key)) {
			requested.add(key);

			JSONObject message = new JSONObject();

			message.put("op", "get-heap-table");
			message.put("address", key);

			PaydayRunnerTask.sendLuaMessage(message.toJSONString());
		}

		return tables.get(key);
	}

	public void apply(JSONObject data, Consumer<VarTable> notifier) {
		String key = (String) data.get("address");
		Var value = Var.fromJSON((JSONObject) data.get("value"));
		VarTable table = (VarTable) value;

		if (!key.equals(table.getAddress())) {
			throw new IllegalStateException("Table does not match address declaration");
		}

		requested.remove(key);

		tables.put(key, table);

		Objects.requireNonNull(table.getChildren(this),
				"Incomplete table sent in furfillment request");

		notifier.accept(table);
	}

	public void clear() {
		tables.clear();
		requested.clear();
	}
}
