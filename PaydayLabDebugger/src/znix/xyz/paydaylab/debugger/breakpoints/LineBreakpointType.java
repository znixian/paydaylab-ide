/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package znix.xyz.paydaylab.debugger.breakpoints;

import javax.swing.JComponent;
import org.netbeans.spi.debugger.ui.BreakpointType;

/**
 *
 * @author ZNix
 */
@BreakpointType.Registration(displayName = "Line")
public class LineBreakpointType extends BreakpointType {

	@Override
	public String getCategoryDisplayName() {
		return "Lua";
	}

	@Override
	public JComponent getCustomizer() {
		return null;
	}

	@Override
	public boolean isDefault() {
		return true;
	}

}
