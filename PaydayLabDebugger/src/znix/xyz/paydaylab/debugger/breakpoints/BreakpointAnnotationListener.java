/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package znix.xyz.paydaylab.debugger.breakpoints;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.Map;
import org.netbeans.api.debugger.Breakpoint;
import org.netbeans.api.debugger.DebuggerManager;
import org.netbeans.api.debugger.DebuggerManagerAdapter;
import org.openide.text.Annotation;
import org.openide.text.AnnotationProvider;
import org.openide.text.Line;
import org.openide.util.Lookup;
import xyz.znix.paydaylab.run.PaydayRunnerTask;

/**
 *
 * @author ZNix
 */
@org.openide.util.lookup.ServiceProvider(service = org.openide.text.AnnotationProvider.class)
public final class BreakpointAnnotationListener extends DebuggerManagerAdapter
		implements PropertyChangeListener, AnnotationProvider {

	private final Map<LuaBreakpoint, Annotation> breakpointToAnnotation = new HashMap<>();

	@Override
	public String[] getProperties() {
		return new String[]{DebuggerManager.PROP_BREAKPOINTS};
	}

	private void updateBreakpoints() {
		if (PaydayRunnerTask.isRunning()) {
			RemoteBreakpointManager.UpdateBreakpoints();
		}
	}

	@Override
	public void breakpointAdded(final Breakpoint b) {
		if (!(b instanceof LuaBreakpoint)) {
			return;
		}
		addAnnotation(b);
		updateBreakpoints();
	}

	@Override
	public void breakpointRemoved(final Breakpoint b) {
		if (!(b instanceof LuaBreakpoint)) {
			return;
		}
		removeAnnotation(b);
		updateBreakpoints();
	}

	@Override
	public void propertyChange(final PropertyChangeEvent evt) {
		String propName = evt.getPropertyName();
		if (propName.equals(Breakpoint.PROP_ENABLED)) {
			removeAnnotation((Breakpoint) evt.getSource());
			addAnnotation((Breakpoint) evt.getSource());
		}
	}

	private void addAnnotation(final Breakpoint b) {
		LuaBreakpoint lb = ((LuaBreakpoint) b);
		Annotation debugAnnotation = new LuaBreakpointAnnotation(lb.getLine(), lb);
		breakpointToAnnotation.put(lb, debugAnnotation);
		b.addPropertyChangeListener(this);
	}

	private void removeAnnotation(Breakpoint b) {
		Annotation annotation = breakpointToAnnotation.remove((LuaBreakpoint) b);
		if (annotation == null) {
			return;
		}
		annotation.detach();
		b.removePropertyChangeListener(this);
	}

	@Override
	public void annotate(Line.Set set, Lookup context) {
		DebuggerManager.getDebuggerManager().getBreakpoints();
	}
}
