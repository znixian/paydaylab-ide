/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package znix.xyz.paydaylab.debugger.breakpoints;

import java.io.File;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.netbeans.api.debugger.Breakpoint;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.text.Line;

/**
 *
 * @author ZNix
 */
public class LuaBreakpoint extends Breakpoint implements JSONAware {

	private final Line line;
	private final FileObject file;

	public LuaBreakpoint(Line line, FileObject fo) {
		this.line = line;
		file = fo;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public void disable() {
	}

	@Override
	public void enable() {
	}

	/**
	 * Returns a representation of what line this breakpoint is on.
	 *
	 * @return
	 */
	public Line getLine() {
		return line;
	}

	/**
	 * Returns a representation of what file this breakpoint is in.
	 *
	 * @return
	 */
	public FileObject getFileObject() {
		return file;
	}

	@Override
	public String toString() {
		if (file != null && line != null) {
			File f = FileUtil.toFile(file);
			return f.getName() + ":" + (line.getLineNumber() + 1);
		}
		return "";
	}

	@Override
	public String toJSONString() {
		JSONObject json = new JSONObject();

		// TODO if owner is null, we've set a breakpoint in the decompiled sources
		// TODO handle that.
		Project owner = FileOwnerQuery.getOwner(file);

		FileObject projectDirectory = owner.getProjectDirectory();
		String relative = FileUtil.getRelativePath(projectDirectory, file);

		relative = "@mods/" + projectDirectory.getName() + "/" + relative;

		json.put("source", relative);
		
		// NetBeans starts lines a 0, Lua at 1
		json.put("line", line.getLineNumber() + 1);

		return json.toJSONString();
	}
}
