/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package znix.xyz.paydaylab.debugger.breakpoints;

import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONObject;
import org.netbeans.api.debugger.Breakpoint;
import org.netbeans.api.debugger.DebuggerManager;
import xyz.znix.paydaylab.run.PaydayRunnerTask;

/**
 *
 * @author ZNix
 */
public class RemoteBreakpointManager {

	private RemoteBreakpointManager() {
	}

	public static void UpdateBreakpoints() {
		DebuggerManager dbg = DebuggerManager.getDebuggerManager();
		List<LuaBreakpoint> breakpoints = new ArrayList<>();
		for (Breakpoint breakpoint : dbg.getBreakpoints()) {
			if (breakpoint instanceof LuaBreakpoint) {
				breakpoints.add((LuaBreakpoint) breakpoint);
			}
		}

		JSONObject root = new JSONObject();
		root.put("op", "breakpoints");
		root.put("breakpoints", breakpoints);

		PaydayRunnerTask.sendLuaMessage(root.toJSONString());
	}
}
