/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package znix.xyz.paydaylab.debugger.breakpoints;

import org.netbeans.api.debugger.Breakpoint;
import org.netbeans.spi.debugger.ui.BreakpointAnnotation;
import org.openide.text.Annotatable;

/**
 *
 * @author ZNix
 */
public class LuaBreakpointAnnotation extends BreakpointAnnotation {

	private final LuaBreakpoint breakpoint;
	private final Annotatable annotatable;

	public LuaBreakpointAnnotation(Annotatable a, LuaBreakpoint b) {
		breakpoint = b;
		annotatable = a;
		attach(a);
	}

	@Override
	public String getAnnotationType() {
		return "Breakpoint";
	}

	@Override
	public String getShortDescription() {
		return "Breakpoint";
	}

	@Override
	public Breakpoint getBreakpoint() {
		return breakpoint;
	}

	@Override
	public String toString() {
		return breakpoint.toString();
	}
}
