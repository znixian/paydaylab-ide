/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package znix.xyz.paydaylab.debugger.breakpoints;

import java.util.Collections;
import java.util.Set;
import javax.swing.SwingUtilities;
import org.netbeans.api.debugger.ActionsManager;
import org.netbeans.api.debugger.Breakpoint;
import org.netbeans.api.debugger.DebuggerManager;
import org.netbeans.spi.debugger.ActionsProvider;
import org.netbeans.spi.debugger.ActionsProviderSupport;
import org.openide.text.Line;
import znix.xyz.paydaylab.debugger.DebuggerUtils;

/**
 *
 * @author ZNix
 */
@ActionsProvider.Registration()
public class BreakpointAction extends ActionsProviderSupport {
	public BreakpointAction() {
		System.out.println("znix.xyz.paydaylab.debugger.breakpoints.BreakpointAction.<init>()");
		setEnabled(ActionsManager.ACTION_TOGGLE_BREAKPOINT, true);
	}

	@Override
	public Set<Object> getActions() {
		return Collections.singleton(ActionsManager.ACTION_TOGGLE_BREAKPOINT);
	}

	@Override
	public void doAction(Object o) {
		SwingUtilities.invokeLater(this::run);
	}

	private void run() {
		Breakpoint[] breakpoints = DebuggerManager.getDebuggerManager().getBreakpoints();
		int i, k = breakpoints.length;

		Line line = DebuggerUtils.getCurrentLine();

		//remove any breakpoints that are necessary.
		for (i = 0; i < k; i++) {
			if (breakpoints[i] instanceof LuaBreakpoint
					&& (((LuaBreakpoint) breakpoints[i]).getLine() != null)
					&& ((LuaBreakpoint) breakpoints[i]).getLine().equals(line)) {

				LuaBreakpoint bp = (LuaBreakpoint) breakpoints[i];
				DebuggerManager.getDebuggerManager().removeBreakpoint(bp);
				break;
			}
		}

		//if we need to, add a breakpoint
		if (i == k) { //add a breakpoint
			LuaBreakpoint bp = DebuggerUtils.getBreakpointAtLine();
			DebuggerManager.getDebuggerManager().addBreakpoint(bp);
		}
	}
}
