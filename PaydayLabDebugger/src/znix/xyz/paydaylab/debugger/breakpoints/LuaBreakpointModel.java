/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package znix.xyz.paydaylab.debugger.breakpoints;

import javax.lang.model.type.UnknownTypeException;
import org.netbeans.spi.viewmodel.ModelListener;
import org.netbeans.spi.viewmodel.NodeModel;

/**
 *
 * @author ZNix
 */
// TODO if this isn't getting used, remove it
public class LuaBreakpointModel implements NodeModel {
	public LuaBreakpointModel() {
		System.out.println("znix.xyz.paydaylab.debugger.breakpoints.LuaBreakpointModel.<init>()");
	}
	
	//Stolen from the PHP debugger in the NetBeans source

	public static final String BREAKPOINT = "org/netbeans/modules/debugger/resources/breakpointsView/NonLineBreakpoint";
	public static final String LINE_BREAKPOINT = "org/netbeans/modules/debugger/resources/breakpointsView/Breakpoint";
	public static final String CURRENT_BREAKPOINT = "org/netbeans/modules/debugger/resources/breakpointsView/NonLineBreakpointHit";
	public static final String CURRENT_LINE_BREAKPOINT = "org/netbeans/modules/debugger/resources/breakpointsView/BreakpointHit";
	public static final String DISABLED_BREAKPOINT = "org/netbeans/modules/debugger/resources/breakpointsView/DisabledNonLineBreakpoint";
	public static final String DISABLED_LINE_BREAKPOINT = "org/netbeans/modules/debugger/resources/breakpointsView/DisabledBreakpoint";
	public static final String DISABLED_CURRENT_BREAKPOINT = "org/netbeans/modules/debugger/resources/breakpointsView/DisabledNonLineBreakpointHit";
	public static final String DISABLED_CURRENT_LINE_BREAKPOINT = "org/netbeans/modules/debugger/resources/breakpointsView/DisabledBreakpointHit";
	public static final String LINE_CONDITIONAL_BREAKPOINT = "org/netbeans/modules/debugger/resources/breakpointsView/ConditionalBreakpoint";
	public static final String CURRENT_LINE_CONDITIONAL_BREAKPOINT = "org/netbeans/modules/debugger/resources/breakpointsView/ConditionalBreakpointHit";
	public static final String DISABLED_LINE_CONDITIONAL_BREAKPOINT = "org/netbeans/modules/debugger/resources/breakpointsView/DisabledConditionalBreakpoint";
	public static final String BROKEN_LINE_BREAKPOINT = "org/netbeans/modules/debugger/resources/breakpointsView/Breakpoint_broken";

	@Override
	public String getDisplayName(Object node) throws UnknownTypeException {
		if (node instanceof LuaBreakpoint) {
			return node.toString();
		}
		return "";
	}

	@Override
	public String getIconBase(Object node) throws UnknownTypeException {
		if (node instanceof LuaBreakpoint) {
			return LINE_BREAKPOINT;
		}
		return "";
	}

	@Override
	public String getShortDescription(Object node) throws UnknownTypeException {
		if (node instanceof LuaBreakpoint) {
			LuaBreakpoint breakpoint = (LuaBreakpoint) node;
			return breakpoint.getLine().getDisplayName();
		}
		return "";
	}

	@Override
	public void addModelListener(ModelListener l) {
	}

	@Override
	public void removeModelListener(ModelListener l) {
	}

}
