/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package znix.xyz.paydaylab.debugger.locals;

import java.util.Map;
import org.json.simple.JSONObject;
import znix.xyz.paydaylab.debugger.LuaHeap;
import znix.xyz.paydaylab.debugger.currentline.CurrentLineManager;

/**
 *
 * @author ZNix
 */
public class Var {

	private final Type type;

	protected Var(Type type) {
		this.type = type;
	}

	public Type getType() {
		return type;
	}

	public Map<String, Var> getChildren(LuaHeap heap) {
		throw new UnsupportedOperationException("Type " + type + " does not support child nodes");
	}

	public static Var fromJSON(JSONObject json) {
		Type type = Type.valueOf(((String) json.get("type")).toUpperCase());
		Object value = json.get("value");

		JSONObject contents;
		if (json.get("contents") instanceof JSONObject) {
			contents = (JSONObject) json.get("contents");
		} else if (json.containsKey("contents")) {
			contents = new JSONObject(); // Empty table
		} else {
			contents = null;
		}

		String address = (String) json.get("address");

		switch (type) {
			// Simple types
			case NIL:
				return new VarNil();
			case BOOLEAN:
				return new VarBoolean((Boolean) value);
			case NUMBER:
				if ("nan".equals(value)) {
					return VarNumber.NAN;
				}
				return new VarNumber(((Number) value).doubleValue());
			case STRING:
				return new VarString((String) value);
			case THREAD:
				return new VarThread();
			// Complex types
			case FUNCTION: {
				return new VarFunction(() -> {
					String source = (String) json.get("source");
					int line = ((Number) json.get("line")).intValue();
					return CurrentLineManager.getLineFromDescription(source, line);
				});
			}
			case USERDATA:
				if (contents != null) {
					return new VarUserdata(address, contents);
				}
				return new VarUserdata(address);
			case TABLE:
				if (contents != null) {
					return new VarTable(address, contents);
				}
				return new VarTable(address);
			default:
				throw new UnsupportedOperationException("Cannot parse type " + type);
		}
	}

	public enum Type {
		NIL(false),
		BOOLEAN(false),
		NUMBER(false),
		STRING(false),
		USERDATA(true),
		FUNCTION(false),
		THREAD(false),
		TABLE(true);

		private final boolean hasChildren;

		private Type(boolean hasChildren) {
			this.hasChildren = hasChildren;
		}

		public boolean hasChildren() {
			return hasChildren;
		}

		@Override
		public String toString() {
			// Title-case
			return name().charAt(0) + name().substring(1).toLowerCase();
		}
	}
}
