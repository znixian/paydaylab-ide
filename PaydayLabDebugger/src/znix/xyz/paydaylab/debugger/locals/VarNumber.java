/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package znix.xyz.paydaylab.debugger.locals;

/**
 *
 * @author ZNix
 */
public class VarNumber extends Var {

	public static final VarNumber NAN = new VarNumber(0) {
		@Override
		public double getValue() {
			return Double.NaN;
		}

		@Override
		public String toString() {
			return "NaN";
		}
	};

	private final double value;

	public VarNumber(double value) {
		super(Type.NUMBER);
		this.value = value;
	}

	public double getValue() {
		return value;
	}

	@Override
	public String toString() {
		return Double.toString(value);
	}
}
