/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package znix.xyz.paydaylab.debugger.locals;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import org.json.simple.JSONObject;
import znix.xyz.paydaylab.debugger.LuaHeap;

/**
 *
 * @author ZNix
 */
public class VarTable extends Var {

	private final Map<String, Var> children;
	private final Map<String, Var> immutableChildren;
	private final String address;

	public VarTable(String address, JSONObject contents) {
		this(Type.TABLE, address, contents);
	}

	public VarTable(String address) {
		this(Type.TABLE, address);
	}

	protected VarTable(Type type, String address, JSONObject contents) {
		super(type);

		this.address = address;

		children = new HashMap<>();
		immutableChildren = Collections.unmodifiableMap(children);

		for (Object name : contents.keySet()) {
			JSONObject elem = (JSONObject) contents.get(name);
			children.put((String) name, Var.fromJSON(elem));
		}
	}

	protected VarTable(Type type, String address) {
		super(type);

		Objects.requireNonNull(address);

		this.address = address;

		children = null;
		immutableChildren = null;
	}

	@Override
	public Map<String, Var> getChildren(LuaHeap heap) {
		VarTable official = heap.getOrRequest(address);

		if (official == null) {
			return null;
		}

		return official.immutableChildren;
	}

	public String getAddress() {
		return address;
	}

	// TODO
}
