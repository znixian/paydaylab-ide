/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package znix.xyz.paydaylab.debugger.locals;

import java.util.*;
import javax.swing.JToolTip;
import org.netbeans.spi.debugger.ContextProvider;
import org.netbeans.spi.viewmodel.*;
import znix.xyz.paydaylab.debugger.*;

import static org.netbeans.spi.debugger.ui.Constants.*;

/**
 *
 * @author ZNix
 */
public class LocalsView implements TreeModel, TableModel, NodeModel {

	private final List<ModelListener> listeners;
	private final DebugSession session;
	private final Map<String, NamedVariable> tableNodes;

	public LocalsView(ContextProvider contextProvider) {
		PaydayDebugger debugger = PaydayDebugger.getDebugger(contextProvider);
		debugger.setLocalsView(this);
		session = debugger.getSession();

		listeners = new ArrayList<>();
		tableNodes = new HashMap<>();
	}

	// NodeModel
	@Override
	public String getDisplayName(Object node) throws UnknownTypeException {
		if (node == ROOT) {
			return "Variables"; // TODO i18n
		}

		NamedVariable var = (NamedVariable) node;
		return var.name;
	}

	@Override
	public String getIconBase(Object node) throws UnknownTypeException {
		return null; // TODO
	}

	@Override
	public String getShortDescription(Object node) throws UnknownTypeException {
		if (node == ROOT) {
			return "Variables"; // TODO i18n
		}

		NamedVariable var = (NamedVariable) node;
		return var.value.getType().toString();
	}

	// TreeModel
	@Override
	public Object getRoot() {
		return ROOT;
	}

	@Override
	public Object[] getChildren(Object node, int from, int to) throws UnknownTypeException {
		if (node != ROOT) {
			Var var = ((NamedVariable) node).value;

			if (!var.getType().hasChildren()) {
				throw new IllegalArgumentException("Cannot get children for type " + var.getType());
			}

			Map<String, Var> children = var.getChildren(session.getHeap());
			if (children == null) {
				children = Collections.emptyMap();
			}

			NamedVariable vars[] = new NamedVariable[children.size()];
			int i = 0;

			for (Map.Entry<String, Var> entry : children.entrySet()) {
				vars[i++] = new NamedVariable(entry.getKey(),
						entry.getValue(), Context.FIELD);
			}

			return vars;
		}

		if (session.isRunning()) {
			return new Object[0];
		}

		LuaFrame frame = session.getStackFrame(-1); // TODO

		Map<String, Var> variables = frame.getVariables();
		Map<String, Var> upvalues = frame.getUpvalues();

		NamedVariable vars[] = new NamedVariable[variables.size() + upvalues.size()];
		int i = 0;

		for (Map.Entry<String, Var> entry : variables.entrySet()) {
			vars[i++] = new NamedVariable(entry.getKey(),
					entry.getValue(), Context.LOCAL);
		}
		for (Map.Entry<String, Var> entry : upvalues.entrySet()) {
			vars[i++] = new NamedVariable(entry.getKey(),
					entry.getValue(), Context.UPVALUE);
		}

		return vars;
	}

	@Override
	public boolean isLeaf(Object node) throws UnknownTypeException {
		if (node == ROOT) {
			return false;
		}

		NamedVariable var = (NamedVariable) node;

		if (var.value instanceof VarTable) {
			String address = ((VarTable) var.value).getAddress();

			tableNodes.put(address, var);

			session.getHeap().getOrRequest(address);
		}

		return !var.value.getType().hasChildren();
	}

	@Override
	public int getChildrenCount(Object node) throws UnknownTypeException {
		if (node == ROOT) {
			if (session.isRunning()) {
				return 0;
			}

			LuaFrame frame = session.getStackFrame(-1); // TODO
			Map<String, Var> variables = frame.getVariables();
			Map<String, Var> upvalues = frame.getUpvalues();
			return variables.size() + upvalues.size();
		}

		Var var = ((NamedVariable) node).value;

		if (!var.getType().hasChildren()) {
			return 0;
		}

		Map<String, Var> children = var.getChildren(session.getHeap());
		return children == null ? 0 : children.size();
	}

	@Override
	public void addModelListener(ModelListener l) {
		listeners.add(l);
	}

	@Override
	public void removeModelListener(ModelListener l) {
		listeners.remove(l);
	}

	public void fireChanges() {
		tableNodes.clear();
		listeners.forEach(l -> l.modelChanged(new ModelEvent.TreeChanged(this)));
	}

	public void fireChangedTable(VarTable changed) {
		NamedVariable node = tableNodes.get(changed.getAddress());

		if (node == null) {
			return;
		}

		ModelEvent event = new ModelEvent.NodeChanged(this, node, ModelEvent.NodeChanged.CHILDREN_MASK);
		listeners.forEach(l -> l.modelChanged(event));
	}

	// TableModel
	@Override
	public Object getValueAt(Object node, String columnID) throws UnknownTypeException {
		if (node == ROOT) {
			System.out.println("znix.xyz.paydaylab.debugger.locals.LocalsView.getValueAt()");
			System.out.println(columnID);
			throw new IllegalStateException(columnID);
		}

		// Hovering over a vaule
		if (node instanceof JToolTip) {
			Object trueNode = ((JToolTip) node).getClientProperty("getShortDescription");
			return getValueAt(trueNode, columnID);
		}

		if (!(node instanceof NamedVariable)) {
			throw new IllegalArgumentException("Bad type " + node);
		}

		NamedVariable var = (NamedVariable) node;

		if (LOCALS_VALUE_COLUMN_ID.equals(columnID)) {
			return var.value.toString();
		} else if (LOCALS_TYPE_COLUMN_ID.equals(columnID)) {
			return var.context + " " + var.value.getType();
		}

		throw new IllegalArgumentException("Bad column " + columnID);
	}

	@Override
	public boolean isReadOnly(Object node, String columnID) throws UnknownTypeException {
		// TODO implement variable editing
		return true;
	}

	@Override
	public void setValueAt(Object node, String columnID, Object value) throws UnknownTypeException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	/**
	 * A representation of a name/variable pair
	 */
	private static class NamedVariable {

		private final String name;
		private final Var value;
		private final Context context;

		public NamedVariable(String name, Var value, Context context) {
			this.name = name;
			this.value = value;
			this.context = context;
		}
	}

	private enum Context {
		LOCAL, UPVALUE, FIELD;
	}
}
