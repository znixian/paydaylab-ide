/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package znix.xyz.paydaylab.debugger.locals;

import org.json.simple.JSONObject;

/**
 *
 * @author ZNix
 */
public class VarUserdata extends VarTable {

	public VarUserdata(String address) {
		super(Type.USERDATA, address);
	}

	VarUserdata(String address, JSONObject jsonObject) {
		super(Type.USERDATA, address, jsonObject);
	}
}
