/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package znix.xyz.paydaylab.debugger.locals;

/**
 *
 * @author ZNix
 */
public class VarBoolean extends Var {

	private final boolean value;

	public VarBoolean(boolean value) {
		super(Type.BOOLEAN);
		this.value = value;
	}

	public boolean getValue() {
		return value;
	}

	@Override
	public String toString() {
		return Boolean.toString(value);
	}
}
