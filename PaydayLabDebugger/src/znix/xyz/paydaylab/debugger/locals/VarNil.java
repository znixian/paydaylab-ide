/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package znix.xyz.paydaylab.debugger.locals;

/**
 *
 * @author ZNix
 */
public class VarNil extends Var {

	public VarNil() {
		super(Type.NIL);
	}

	@Override
	public String toString() {
		return "Nil"; // TODO i18n
	}
}
